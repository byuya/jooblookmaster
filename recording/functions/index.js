
'use strict';

const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();
const logging = require('@google-cloud/logging');
const stripe = require('stripe')(functions.config().stripe.token);
const currency = functions.config().stripe.currency || 'JPY';

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions

exports.helloWorld = functions.https.onRequest((request, response) => {
 response.send("Hello from Firebase!");
});

// When a user is created, register them with Stripe
//1/04/2019
// exports.createStripeCustomer = functions.auth.user().onCreate((user) => {
//   return stripe.customers.create({
//     email: user.email,
//   }).then((customer) => {
//     return admin.database().ref(`/users/stripe/${user.uid}/customer_id`).set(customer.id);
//   });
// });


//new function using async/await 2/13
exports.createStripeCustomer = functions.auth.user().onCreate(async (user) => {
  try {
    const customer = await stripe.customers.create({
      email: user.email});
      return admin.database().ref(`/users/stripe/${user.uid}/customer_id`).set(customer.id);
  } catch (error) {
    console.log("error has occured", error)
    return error
  }
});

// Add a payment source (card) for a user by writing a stripe payment source token to Realtime database
//1/04/2019
// exports.addPaymentSource = functions.database
//     .ref('/users/{userId}/sources/{pushId}/token').onWrite((change, context) => {
//       const source = change.after.val();
//       if (source === null){
//         return null;
//       }
//       return admin.database().ref(`/users/stripe/${context.params.userId}/customer_id`)
//           .once('value').then((snapshot) => {
//             return snapshot.val();
//           }).then((customer) => {
//             return stripe.customers.createSource(customer, {source});
//           }).then((response) => {
//             return change.after.ref.parent.set(response);
//           }, (error) => {
//             return change.after.ref.parent.child('error').set(userFacingMessage(error));
//           }).then(() => {
//             return reportError(error, {user: context.params.userId});
//           });
//         });

//new function using async/await 2/13
exports.addPaymentSource = functions.database
    .ref('/users/{userId}/sources/{pushId}/token').onWrite(async (change, context) => {
      const userId = context.params.userId;
      const source = change.after.val()
      if (source === null) {
        return null;
      }

      try {
        const snapshot = await admin.database().ref(`/users/stripe/${context.params.userId}/customer_id`)
          .once('value');
        const customer = snapshot.val();
        console.log("this is customer", customer)
        const response = await stripe.customers.createSource(customer, {source});
        console.log("response from Stripe", response);

        return change.after.ref.parent.set(response)

      } catch (error) {

        console.log("this is error", error);

       //  const fetchUid = admin.database().ref('/users/' + userId).once('value').then((snaps) => {
       //    var fromId = snaps.val();
       //    console.log("this is senderId", fromId);
       //    return fromId
       //  });
       //
       //    var message = {
       //      //data parameter only accepts string values
       //     data: {
       //       fromId: userId,
       //       statusCode: "402",
       //       errorMessage: error.message
       //     },
       //
       //     // apns: {
       //     //   payload: {
       //     //     aps: {
       //     //       alert: {
       //     //         title: 'You got a comment from ' + senderId.username,
       //     //         body: text
       //     //       },
       //     //
       //     //       //badge: Number(readCount),
       //     //       "content-available": 1
       //     //     }
       //     //   }
       //     // },
       //
       //     token: "erczLW1JIz0:APA91bFaGWNx6PrmxF4mK3o7kz6C9JvplPR_qK-XBhXEgTmIPVFRoBgjZd4ebcm0Y0olQwK6idovq1TK1VsGHcEqWv5mzAzkP2unnjasZlpXJW7B-FBirUh_4nra6zipwp5NmgHssPtwwRru1z8AgsTBflOufL70uA"
       //   };
       //
       // const sendMessage = admin.messaging().send(message);

       return change.after.ref.parent.child('error').set(userFacingMessage(error));
       //return reportError(error, {user: context.params.userId});
     }

    })

        // [START chargecustomer]
        // Charge the Stripe customer whenever an amount is written to the Realtime database
        // exports.createStripeCharge = functions.database.ref('/users/{userId}/charges/{id}')
        //     .onCreate((snap, context) => {
        //       const val = snap.val();
        //       console.log(snap.val())
        //       // Look up the Stripe customer id written in createStripeCustomer
        //       return admin.database().ref(`/users/stripe/${context.params.userId}/customer_id`)
        //           .once('value').then((snapshot) => {
        //             return snapshot.val();
        //           }).then((customer) => {
        //             // Create a charge using the pushId as the idempotency key
        //             // protecting against double charges
        //             const amount = val.amount;
        //             const idempotencyKey = context.params.id;
        //             const description = "this is test charge";
        //             const key = val.key;
        //
        //             const charge = {amount, currency, description, customer, metadata: {key: key}};
        //             //, destination: {amount: 500, account: "acct_1DGpJAB4eRH9YEfI"}
        //             if (val.source !== null) {
        //               charge.source = val.source;
        //             }
        //             return stripe.charges.create(charge, {idempotency_key: idempotencyKey});
        //           }).then((response) => {
        //             // If the result is successful, write it back to the database
        //             return snap.ref.set(response);
        //           }).catch((error) => {
        //             // We want to capture errors and render them in a user-friendly way, while
        //             // still logging an exception with StackDriver
        //             return snap.ref.child('error').set(userFacingMessage(error));
        //           }).then(() => {
        //             return reportError(error, {user: context.params.userId});
        //           });
        //         });

        exports.observeComments = functions.database.ref('/comments/{postId}/{pushId}/')
          .onCreate((snapshot, context) => {
            const uid = snapshot.val().uid;
            const toId = snapshot.val().toId;
            const text = snapshot.val().text;
            const creationDate = snapshot.val().creationDate;
            const postUser = snapshot.val().postUser;
            const postId = context.params.postId;

            console.log('User: ', uid, 'is sending to', toId, 'text', text);

             const fetchUid = admin.database().ref('/users/' + uid).once('value').then((snap) => {
               var fromId = snap.val();
               console.log("this is fromId", fromId)
               return fromId
             })

             const fetchToId = admin.database().ref('/users/' + toId).once('value').then((snaps) => {
               var recipientId = snaps.val();
               console.log("this is recipientId", recipientId)
               return recipientId
             })

             const updateToIdCount = admin.database().ref('/count/' + toId).child('read')
              .transaction((userReadCount) => {
               return (userReadCount || 0) + 1
             })

             var date = {creationDate: creationDate};

             console.log("this is updatedate", date, "postUser", postUser, "postId", postId);

             const updateToIdDate = admin.database().ref('/user-comment/' + toId + '/' + uid + '/' + postUser + '/' + postId)
              .update(date).then((updated) => {
                return updated;
             })

             const updateFromIdDate = admin.database().ref('/user-comment/' + uid + '/' + toId + '/' + postUser + '/' + postId)
              .update(date).then((updated) => {
                return updated;
          })

            const updateTotalReadCountToId = admin.database().ref('/user-comment/' + toId).child('totalCount')
              .transaction((totalCountToId) => {
                  return (totalCountToId || 0) + 1
                  })

          //           var countDataToId = updatedtotalCountToId.snapshot.val();
          //             console.log("this is updatedCommenDataToId", countDataToId)
          //
            const updateTotalReadCountFromId = admin.database().ref('/user-comment/' + uid).child('totalCount')
              .transaction((totalCountFromId) => {
                    return totalCountFromId + 0
                  })

            const updateEachReadCountToId = admin.database().ref('/user-comment/' + toId + '/' + uid + '/' + postUser + '/' + postId)
              .child('read').transaction((commentToIdCount) => {
                      return (commentToIdCount || 0) + 1
                  })

            const updateEachReadCountFromId = admin.database().ref('/user-comment/' + uid + '/' + toId + '/' + postUser + '/' + postId)
              .child('read').transaction((commentUidCount) => {
                        return commentUidCount + 0
                  })

                  return Promise.all([fetchUid, fetchToId, updateToIdCount,
                                      updateToIdDate, updateFromIdDate, updateTotalReadCountToId,
                                      updateTotalReadCountFromId, updateEachReadCountToId, updateEachReadCountFromId])
                    .then((results) => {

                      const senderName = results[0].username;
                      const recipientName = results[1].username;
                      const recipientFcmToken = results[1].fcmToken;
                      const totalCountOfRecipient = results[2].snapshot.val();
                      const totalReadCountOfRecipient = results[5].snapshot.val();

                      console.log("senderName", senderName, "recipientName", recipientName,
                      "totalCount", totalCountOfRecipient, "totalReadCount", totalReadCountOfRecipient)

                                var message = {
                                  //data parameter only accepts string values
                                 data: {
                                   fromId: uid,
                                   badge: totalCountOfRecipient.toString(),
                                   commentBadge: totalReadCountOfRecipient.toString()
                                 },

                                 apns: {
                                   payload: {
                                     aps: {
                                       alert: {
                                         title: 'You got a comment from ' + senderName,
                                         body: text
                                       },

                                       //badge: Number(readCount),
                                       "content-available": 1
                                     }
                                   }
                                 },

                                 token: recipientFcmToken
                               };
                    //return results
                    return admin.messaging().send(message)
                    }).then((response) => {
                      console.log('Successfully sent message:', response);
                      return response;

                    //return results
                  }).catch((error) => {
                    console.log("error has occured", error)
                  })
        })

//new function using async/await 2/13
        exports.createStripeCharge = functions.database.ref('/experiment/{id}')
          .onCreate(async (snap, context) => {
              const val = snap.val();
              const uid = val.uid;
              const hostId = val.hostId;
              const expId = context.params.id

              console.log('uid:', uid, 'hostId:', hostId)

              try {
                  // Look up the Stripe customer id written in createStripeCustomer
                const snapshot = await admin.database().ref('/users/stripe/' + uid + '/customer_id')
                    .once('value')
                    const snapval = snapshot.val();
                    const customer = snapval;
                    // Create a charge using the pushId as the idempotency key
                    // protecting against double charges
                  const amount = val.amount;
                  const idempotencyKey = context.params.id;
                  const description = "this is test charge";
                  const key = val.key;
                  const accountId = val.accountId;
                  const caption = val.caption;
                  const place = val.place;
                  const title = val.title;

                const charge = {amount, currency, description, customer,
                  metadata: {key: key, accountId: accountId, title: title, caption: caption, place: place, cancelRequest: 0, read: 1}};

                  if (val.source !== null) {
                    charge.source = val.source;
                  }
                  const response = await stripe.charges.create(charge, {idempotency_key: idempotencyKey});
                  //if the result is successful, write the response back to firebase database
                  //And send message to host cloud messaging
                  console.log('charge response', response);

                    return snap.ref.set(response).then(() => {

                    const fetchHostId = admin.database().ref('/users/' + hostId).once('value').then((snap) => {
                      return snap.val();
                    })

                    const fetchGuestId = admin.database().ref('/users/' + uid).once('value').then((snaps) => {
                      return snaps.val();
                    })

                    var set = {test: 1}

                    const experimentGuest = admin.database().ref('/experiment-guest/' + uid + '/' + hostId)
                    .child(expId).set(set)

                    const experimentHost = admin.database().ref('/experiment-host/' + hostId + '/' + uid)
                    .child(expId).set(set)

                    const settingMessageHost = admin.database().ref('/user-messages/' + hostId + '/' + uid).child('read')
                      .transaction((settingDone) => {
                        return settingDone + 0
                      })

                    const settingMessageGuest = admin.database().ref('/user-messages/' + uid + '/' + hostId).child('read')
                      .transaction((settingDone) => {
                        return settingDone + 0
                      })

                    const incCountHost = admin.database().ref('/count/' + hostId).child('read')
                      .transaction((incDone) => {
                        return (incDone || 0) + 1
                      })

                    return Promise.all([fetchHostId, fetchGuestId, experimentGuest,
                                        experimentHost, settingMessageHost, settingMessageGuest,
                                        incCountHost]).then((results) => {

                        const hostName = results[0].username;
                        const hostFcmToken = results[0].fcmToken;
                        const guestName = results[1].username;
                        const totalCountOfHost = results[6].snapshot.val();

                        console.log("hostName", hostName, "guestName", guestName, "total", totalCountOfHost)

                          var message = {
                              //data parameter only accepts string values
                              data: {
                                fromId: uid,
                                badge: totalCountOfHost.toString(),
                                //commentBadge: totalReadCountOfRecipient.toString()
                                    },

                              apns: {
                                payload: {
                                  aps: {
                                    alert: {
                                      title: "Congratulations!",
                                      body: guestName + "bought your event"
                                            },

                                            //badge: Number(readCount),
                                      "content-available": 1
                                        }
                                      }
                                    },

                            token: hostFcmToken

                                      };

                        return admin.messaging().send(message)

                      })

                  });

                } catch (error) {
                  console.log('this is error', error);
                  await snap.ref.child('error').set(userFacingMessage(error));
                  return reportError(error, {user: uid});
              }
          })

          //1/04/2019
          // exports.observeExperience = functions.database.ref('/experiment/{pushId}/')
          //   .onCreate((snapshot, context) => {
          //
          //     const val = snapshot.val();
          //     var uid = val.uid;
          //     var hostId = val.hostId;
          //     var pushId = context.params.pushId;
          //
          //     console.log('User: ', uid, 'purchased your experience', hostId);
          //
          //     return admin.database().ref('/users/' + hostId).once('value', snapshot => {
          //
          //       var host = snapshot.val();
          //
          //     return admin.database().ref('/users/' + uid).once('value', snapshot => {
          //
          //       var purchase = snapshot.val();
          //
          //     return admin.database().ref('/experiment-guest/' + uid + '/' + hostId).child(pushId).set({
          //       test: 1,
          //     }).then((createdExpIdToUid) => {
          //       return admin.database().ref('/experiment-host/' + hostId + '/' + uid).child(pushId).set({
          //         test: 1,
          //       }).then((createdExpIdToHostId) => {
          //
          //     return admin.database().ref('/user-messages/' + hostId + '/' + uid).child('read').transaction((settingReadToHost) => {
          //       return settingReadToHost + 0
          //     }).then((hostIdSettingCount) => {
          //
          //     return admin.database().ref('/user-messages/' + uid + '/' + hostId).child('read').transaction((settingReadToGuest) => {
          //       return settingReadToGuest + 0
          //     }).then((guestIdSettingCount) => {
          //
          //       return admin.database().ref('/count/' + hostId).child('read').transaction((userReadCount) => {
          //         return (userReadCount || 0) + 1
          //       }).then((readTotalCount) => {
          //
          //         var summedCount = readTotalCount.snapshot.val();
          //         console.log('check count when guest purchased event', summedCount)
          //
          //       var message = {
          //        // notification: {
          //        //   title: 'Congratulations!',
          //        //   body: purchase.username + 'You event was bought'
          //        // },
          //
          //        data: {
          //          fromId: uid,
          //          badge: summedCount.toString()
          //        },
          //
          //        apns: {
          //         payload: {
          //            aps: {
          //               alert: {
          //                 title: 'Congratulations!',
          //                 body: purchase.username + 'bought your event!'
          //                  },
          //
          //                  //badge: Number(readCount),
          //                  "content-available": 1
          //                }
          //              }
          //            },
          //
          //        token: host.fcmToken
          //      };
          //
          //       return admin.messaging().send(message)
          //         .then((response) => {
          //           console.log('Successfully sent message:', response);
          //           return response;
          //         })
          //         .catch((error) => {
          //           console.log('Error sending message:', error);
          //           //throw new error('Error sending message:', error);
          //         });
          //     })
          //     })
          //   })
          // })
          //
          //   })
          //   })
          //   })
          //   })
                //1/04/2019
                // exports.createStripeCharge = functions.database.ref('/experiment/{id}')
                //     .onCreate((snap, context) => {
                //       const val = snap.val();
                //       const uid = val.uid;
                //       const hostId = val.hostId;
                //
                //       console.log('uid:', uid, 'hostId:', hostId)
                //
                //       // Look up the Stripe customer id written in createStripeCustomer
                //       return admin.database().ref('/users/stripe/' + uid + '/customer_id')
                //           .once('value').then((snapshot) => {
                //             return snapshot.val();
                //           }).then((customer) => {
                //             // Create a charge using the pushId as the idempotency key
                //             // protecting against double charges
                //             const amount = val.amount;
                //             const idempotencyKey = context.params.id;
                //             const description = "this is test charge";
                //             const key = val.key;
                //             const accountId = val.accountId;
                //             const caption = val.caption;
                //             const place = val.place;
                //             const title = val.title;
                //
                //             const charge = {amount, currency, description, customer,
                //               metadata: {key: key, accountId: accountId, title: title, caption: caption, place: place, cancelRequest: 0, read: 1}};
                //             //, destination: {amount: 500, account: "acct_1DGpJAB4eRH9YEfI"}
                //             if (val.source !== null) {
                //               charge.source = val.source;
                //             }
                //             return stripe.charges.create(charge, {idempotency_key: idempotencyKey});
                //           }).then((response) => {
                //             // If the result is successful, write it back to the database
                //             return snap.ref.set(response);
                //           }).catch((error) => {
                //
                //             console.log("this is error", error)
                //
                //             // We want to capture errors and render them in a user-friendly way, while
                //             // still logging an exception with StackDriver
                //             return snap.ref.child('error').set(userFacingMessage(error));
                //           }).then(() => {
                //             return reportError(error, {user: uid});
                //           });
                //         });

        // [END chargecustomer]]

//2/19 new createcustomaccount using async await
exports.createCustomAccount = functions.database.ref('/users/{userId}/host')
  .onCreate(async(snap, context) => {
    const val = snap.val();

    try {
      const snapshot = await admin.database().ref(`/users/stripe/${context.params.userId}/customer_id`).once('value')
      const snapVal = snapshot.val();
      const customer = snapVal;
      if (customer === null) {
        console.log("there's no stripe customer");
        return null;
      }

      const firstName = val.firstName;
      const lastName = val.lastName;
      const gender = val.gender;
      const type = val.type;
      const phone = val.phone;
      const dateBirthYear = val.dateBirthYear;
      const dateBirthMonth = val.dateBirthMonth;
      const dateBirthDay = val.dateBirthDay;
      const stateKanji = val.stateKanji;
      const cityKanji = val.cityKanji;
      const townKanji = val.townKanji;
      const stateKana = val.stateKana;
      const cityKana = val.cityKana;
      const townKana = val.townKana;
      const number = val.number;
      const postal = val.postal;
      const doc = val.doc;

      const response = await stripe.accounts.create({

        type: type,

        legal_entity: {
          first_name_kana: firstName,
          last_name_kana: lastName,
          first_name_kanji: firstName,
          last_name_kanji: lastName,
          type: "individual",
          phone_number: phone,
          gender: gender,

        dob: {
          day: dateBirthDay,
          month: dateBirthMonth,
          year: dateBirthYear,
        },

        address_kana: {
          state: stateKana,
          city: cityKana,
          town: townKana,
          line1: number,
          postal_code: postal,
        },

        address_kanji: {
          state: stateKanji,
          city: cityKanji,
          town: townKanji,
          line1: number,
          postal_code: postal,
        },

        verification: {
          document: doc, //"file_1DGmMxIWuXIpVcpJtSHTDww5" dummy22's file_id
        }

      },

      payout_schedule: {
        interval: "manual",
      },

      tos_acceptance: {
        date: Math.floor(Date.now() / 1000), //Time.now.to_i //Math.floor(Date.now() / 1000) //1510637345
        ip: "126.153.85.198", //"192.168.0.1" request.connection.remoteAddress
      }

      })

      return snap.ref.set(response);

    } catch (error) {
      return snap.ref.child('error').set(userFacingMessage(error));
    }
  })

//1/04/2019
// exports.createCustomAccount = functions.database
//     .ref('/users/{userId}/host').onCreate((snap, context) => {
//     const val = snap.val();
//
//       console.log(snap.val())
//   return admin.database().ref(`/users/${context.params.userId}/customer_id`)
//     .once('value').then((snapshot) => {
//       return snapshot.val();
//     }).then((customer) => {
//       const firstName = val.firstName;
//       const lastName = val.lastName;
//       const gender = val.gender;
//       const type = val.type;
//       const phone = val.phone;
//       const dateBirthYear = val.dateBirthYear;
//       const dateBirthMonth = val.dateBirthMonth;
//       const dateBirthDay = val.dateBirthDay;
//       const stateKanji = val.stateKanji;
//       const cityKanji = val.cityKanji;
//       const townKanji = val.townKanji;
//       const stateKana = val.stateKana;
//       const cityKana = val.cityKana;
//       const townKana = val.townKana;
//       const number = val.number;
//       const postal = val.postal;
//       const doc = val.doc;
//
//       return stripe.accounts.create(
//
//         { //support_phone: "055-0000-1111",
//           type: type,
//
//           legal_entity: {
//             first_name_kana: firstName,
//             last_name_kana: lastName,
//             first_name_kanji: firstName,
//             last_name_kanji: lastName,
//             type: "individual",
//             phone_number: phone,
//             gender: gender,
//
//           dob: {
//             day: dateBirthDay,
//             month: dateBirthMonth,
//             year: dateBirthYear,
//           },
//
//           address_kana: {
//             state: stateKana,
//             city: cityKana,
//             town: townKana,
//             line1: number,
//             postal_code: postal,
//           },
//
//           address_kanji: {
//             state: stateKanji,
//             city: cityKanji,
//             town: townKanji,
//             line1: number,
//             postal_code: postal,
//           },
//
//           verification: {
//             document: doc, //"file_1DGmMxIWuXIpVcpJtSHTDww5" dummy22's file_id
//           }
//
//         },
//
//         payout_schedule: {
//           interval: "manual",
//         },
//
//         tos_acceptance: {
//           date: Math.floor(Date.now() / 1000), //Time.now.to_i //Math.floor(Date.now() / 1000) //1510637345
//           ip: "126.153.85.198", //"192.168.0.1" request.connection.remoteAddress
//         }
//       });
//
//     }).then((response) => {
//       return snap.ref.set(response);
//     }).catch((error) => {
//       return snap.ref.child('error').set(userFacingMessage(error));
//     }).then(() => {
//       return reportError(error, {user: context.params.userId});
//     });
//   });
//
// ///1/04/2019
exports.createBankAccount = functions.database
      .ref('/users/{userId}/bank/{pushId}/token').onWrite((change, context) => {
        const external_account = change.after.val();
        console.log(external_account);
        if (external_account === null){
          return null;
        }
        //var autoId = "-LPVTSvfJeU-lNZTPkUv";
        return admin.database().ref(`/users/${context.params.userId}/host/id`)
            .once('value').then((snapshot) => {
              //console.log(snapshot.val());
              return snapshot.val();

            }).then((account) => {
              console.log(account);
              return stripe.accounts.createExternalAccount(account, {external_account});
            }).then((response) => {
              return change.after.ref.parent.set(response);
            }, (error) => {
              return change.after.ref.parent.child('error').set(userFacingMessage(error));
            }).then(() => {
              return reportError(error, {user: context.params.userId});
            });
          });

//this function is called whenever a host privides a feedback of the service.
//1/04/2019
exports.trasferAfterFeedback = functions.database.ref('/users/{userId}/transfers/{transfer_id}')
      .onCreate((snap, context) => {
          const val = snap.val();
          console.log(snap.val())
          // Look up the Stripe account id written in createStripeCustomer
          return admin.database().ref(`/users/${context.params.userId}/host/id`)
          .once('value').then((snapshot) => {
            return snapshot.val();
            }).then((account) => {
          // Create a charge using the pushId as the idempotency key
          // protecting against double charges
              const amount = val.amount;
              const idempotencyKey = context.params.id;
              const description = "this is a transfer";
              const destination = val.destination;
              const chargeId = val.chargeId;
              const source_transaction = val.source_transaction;
              const transfer = {amount, currency, description, destination, source_transaction};

                  if (val.source !== null) {
                    transfer.source = val.source;
                        }
                      return stripe.transfers.create(transfer, {idempotency_key: idempotencyKey});
                      }).then((response) => {
                      // If the result is successful, write it back to the database
                      return snap.ref.set(response);
                      }).catch((error) => {
                      // We want to capture errors and render them in a user-friendly way, while
                      // still logging an exception with StackDriver
                      return snap.ref.child('error').set(userFacingMessage(error));
                      }).then(() => {
                      return reportError(error, {user: context.params.userId});
                        });
                      });

//this functins is called whenever a host or a guest cancells a service.
//1/04/2019
  exports.refundRequestbyUser = functions.database
    .ref('/users/{userId}/refund/{pushId}/charge').onWrite((change, context) => {
        const charge = change.after.val();
          if (charge === null){
            return null;
          }
            return admin.database().ref(`/users/${context.params.userId}/host/id`)
            .once('value').then((snapshot) => {
              return snapshot.val();
                }).then((account) => {
                  return stripe.refunds.create({charge: charge});
                  }).then((response) => {
                    return change.after.ref.parent.set(response);
                  }, (error) => {
                    return change.after.ref.parent.child('error').set(userFacingMessage(error));
                  }).then(() => {
                    return reportError(error, {user: context.params.userId});
                  });
                  });

//1/04/2019
exports.refundRequestByHost = functions.database
  .ref('/users/{guestId}/refund_host/{userId}/{pushId}/').onCreate((snap, context) => {

    const charge = snap.val().charge;
    var guestId = context.params.guestId;
    var userId = context.params.userId;

    if (charge === null) {
      return null;
    }

    console.log('guestId: ', guestId, 'userId: ', userId, 'this is chargeId: ', charge);

    return admin.database().ref(`/users/${context.params.userId}/host/id`)
    .once('value').then((snapshot) => {
      return snapshot.val();
        }).then((account) => {
          return stripe.refunds.create({charge: charge});
          }).then((response) => {
            return snap.ref.set(response);
          }, (error) => {
            return snap.ref.child('error').set(userFacingMessage(error));
          }).then(() => {
            return reportError(error, {user: context.params.userId});
          });
          });

          //1/04/2019
          exports.payoutToCustomAccount =  functions.database
            .ref('/users/{userId}/payout/{pushId}/').onCreate((snap, context) => {
              //const amount = snap.val().amount;
              //const destination = snap.val().destination;
              const val = snap.val();

              //console.log("this is amount", amount, "this is bankid", destination);

              return admin.database().ref(`/users/${context.params.userId}/host/id`)
                .once('value').then((snapshot) => {
                  return snapshot.val();
                }).then((account) => {
                    const amount = val.amount;
                    const destination = val.destination;
                    const currency = "jpy";
                    const source_type = "bank_account";

                    return stripe.payouts.create({
                      amount: amount,
                      currency: currency,
                      destination: destination},
                      {stripe_account: account,

                    });
                  }).then((response) => {
                    return snap.ref.set(response);
                  }).catch((error) => {
                    return snap.ref.child('error').set(userFacingMessage(error));
                  }).then(() => {
                    return reportError(error, {user: context.params.userId});
                  });
                });


exports.observeMessages = functions.database.ref('/messages/{messageId}/')
  .onCreate((snapshot, context) => {

    const fromId = snapshot.val().fromId;
    const toId = snapshot.val().toId;
    const messageText = snapshot.val().messageText;
    const messageId = context.params.messageId;

    console.log('User: ', fromId, 'is sending to', toId, 'messageId', messageId);

    const fetchToid = admin.database().ref('/users/' + toId).once('value').then((snap) => {
      var recipientId = snap.val();
      console.log("this is recipeientId", recipientId);
      return recipientId
    })

    const fetchUid = admin.database().ref('/users/' + fromId).once('value').then((snaps) => {
      var senderId = snaps.val();
      console.log("this is senderId", senderId)
      return senderId
    })

    const incrementCountToId = admin.database().ref('/count/' + toId).child("read").transaction((updateCountToId) => {
      return (updateCountToId || 0) + 1;
    })

    const incrementTotalMesCountToId = admin.database().ref('/user-messages/' + toId).child('totalCount')
      .transaction((updateTotalCountToId) => {
      return (updateTotalCountToId || 0) + 1;
    })

    const incrementTotalMesCountFromId = admin.database().ref('/user-messages/' + fromId).child('totalCount')
      .transaction((updateTotalCountFromId) => {
      return updateTotalCountFromId + 0
    })

    const incToIdReadCount = admin.database().ref('/user-messages/' + toId + '/' + fromId).child('read')
      .transaction((updateToIdReadCount) => {
        return (updateToIdReadCount || 0) + 1
      })

    const makefromIdReadCount = admin.database().ref('/user-messages/' + fromId + '/' + toId).child('read')
      .transaction((makeFromIdReadCount) => {
        return makeFromIdReadCount + 0
      })

      return Promise.all([fetchToid, fetchUid, incrementCountToId,
                          incrementTotalMesCountToId, incrementTotalMesCountFromId,
                          incToIdReadCount, makefromIdReadCount])
        .then((results) => {

          // //renderResults ({
          //   toId: results[0];
          //   fromId: results[1];
          // //})

        const recipientName = results[0].username;
        const recipientFcmToken = results[0].fcmToken;
        const senderName = results[1].username;
        const countOfRecipient = results[2].snapshot.val();
        const countTotalOfRecipient = results[3].snapshot.val();

        console.log("recipientName", recipientName, "senderName", senderName, "count", countOfRecipient, "totalCount", countTotalOfRecipient)
        //console.log("all functions worked properly", results[2])

        var message = {
            //data parameter only accepts string values
            data: {
                fromId: fromId,
                badge: countOfRecipient.toString(),
                messageBadge: countTotalOfRecipient.toString()
                   },

                   apns: {
                     payload: {
                       aps: {
                         alert: {
                           title: 'You got a message from ' + senderName,
                           body: messageText
                         },

                         //badge: Number(readCount),
                         "content-available": 1
                       }
                     }
                   },

                   token: recipientFcmToken
                 };

      return admin.messaging().send(message)
      }).then((response) => {
        console.log('Successfully sent message:', response);
        return response;

      //return results
    }).catch((error) => {
      console.log("error has occured", error)
    })
  })

//listen for following events and then trigger a push notification
//1/04/2019
exports.observeFollowing = functions.database.ref('/following/{uid}/{followingId}')
     .onCreate((snapshot, context) => {

     //const original = snapshot.val();

     var uid = context.params.uid
     var followingId = context.params.followingId

    console.log('User: ', uid, 'is following ', followingId);

     // lets log out some messages
    //return admin.database().ref('/users/', followingId).once('value').then(function(snapshot) {
    return admin.database().ref('/users/' + followingId).once('value', snapshot => {

     var userWeAreFollowing = snapshot.val();

     //return admin.database().ref('/users/', uid).once('value').then(function(snapshot) {
    return admin.database().ref('/users/' + uid).once('value', snapshot => {

     var userDoingTheFollowing = snapshot.val();

     var message = {
      notification: {
        title: "You now have a new Follower",
        body: userDoingTheFollowing.username + ' is now following you'
      },

      data: {
        followerId: uid
      },

      token: userWeAreFollowing.fcmToken
    };

     admin.messaging().send(message)
       .then((response) => {
         console.log('Successfully sent message:', response);
         return response;
       })
       .catch((error) => {
         console.log('Error sending message:', error);
         //throw new error('Error sending message:', error);
       });
   })
   })
 })
//1/04/2019
exports.sendPushNotifications = functions.https.onRequest((request, response) => {
  response.send("Attempting to send push notifications")
  console.log("LOGGER --- Trying to send push message...")

  // This registration token comes from the client FCM SDKs.
  //registrationToken is fcm token from Firebase

var uid = 'QLyuD4A38CeMYtt413V0XQ5V6f92';

return admin.database().ref('/users/' + uid).once('value', snapshot => {

  var user = snapshot.val();

  console.log("User username: " + user.username + " fcmToken: " + user.fcmToken);

   var message = {
    notification: {
      title: "Push Notification TITLE HERE",
      body: "I want to make sure this is working Mr.Bag"
    },
    data: {
      score: '850',
      time: '2:45'
    },
    token: user.fcmToken
  };

  admin.messaging().send(message)
    .then((response) => {
      // Response is a message ID string.
      console.log('Successfully sent message:', response);
      return response;
    })
    .catch((error) => {
      console.log('Error sending message:', error);
      //throw new Error('Error sending message:', error);
    })

})
// var fcmToken = "erczLW1JIz0:APA91bFaGWNx6PrmxF4mK3o7kz6C9JvplPR_qK-XBhXEgTmIPVFRoBgjZd4ebcm0Y0olQwK6idovq1TK1VsGHcEqWv5mzAzkP2unnjasZlpXJW7B-FBirUh_4nra6zipwp5NmgHssPtwwRru1z8AgsTBflOufL70uA";
//
// // See documentation on defining a message payload.
// var message = {
//   notification: {
//     title: "Push Notification TITLE HERE",
//     body: "body over here for message"
//   },
//   data: {
//     score: '850',
//     time: '2:45'
//   },
//   token: fcmToken
// };
//
// // Send a message to the device corresponding to the provided
// // registration token.
})

// To keep on top of errors, we should raise a verbose error report with Stackdriver rather
// than simply relying on console.error. This will calculate users affected + send you email
// alerts, if you've opted into receiving them.
// [START reporterror]

//1/04/2019
 function reportError(err, context = {}) {
  // This is the name of the StackDriver log stream that will receive the log
  // entry. This name can be any valid log stream name, but must contain "err"
  // in order for the error to be picked up by StackDriver Error Reporting.
  const logName = 'errors';
  const log = logging.log(logName);

  // https://cloud.google.com/logging/docs/api/ref_v2beta1/rest/v2beta1/MonitoredResource
  const metadata = {
    resource: {
      type: 'cloud_function',
      labels: {function_name: process.env.FUNCTION_NAME},
    },
  };

  // https://cloud.google.com/error-reporting/reference/rest/v1beta1/ErrorEvent
  const errorEvent = {
    message: err.stack,
    serviceContext: {
      service: process.env.FUNCTION_NAME,
      resourceType: 'cloud_function',
    },
    context: context,
  };

  // Write the error log entry
  return new Promise((resolve, reject) => {
    log.write(log.entry(metadata, errorEvent), (error) => {
      if (error) {
       return reject(error);
      }
      return resolve();
    });
  });
}
// [END reporterror]

// Sanitize the error message for the user
function userFacingMessage(error) {
  return error.type ? error.message : 'An error occurred, developers have been alerted';
}
