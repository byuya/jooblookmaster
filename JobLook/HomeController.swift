//
//  HomeController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/07/24.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth

class HomeController: UICollectionViewController, UICollectionViewDelegateFlowLayout, HomePostCellDelegate {
    
    let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //(Episode 28) how to load post automatically: part2
        //let name = NSNotification.Name(rawValue: "UpdateFeed")
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: SharePhotoController.updateFeedNotificationName, object: nil)

        collectionView?.backgroundColor = .white
        
        collectionView?.register(HomePostCell.self, forCellWithReuseIdentifier: cellId)
        
        //start refreshing
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
        
//        NotificationCenter.default.addObserver(self, selector: #selector(willResignActive), name: UIApplication.willResignActiveNotification, object: nil)
        
        setupNavigationItem()
     
        fetchAllPosts()
    }
    

//    @objc func willResignActive() {
//        print("check if the app went to background")
//    }
    
    @objc func handleRefresh() {
        print("Handling refresh...")
        //remove all posts if unfollowed some accounts
        posts.removeAll()
        
        fetchAllPosts()
    }
    
    @objc func handleUpdateFeed() {
        handleRefresh()
    }
    
    fileprivate func fetchAllPosts() {
        fetchPosts()
        fetchFollowingUserIds()
    }
    
    fileprivate func fetchFollowingUserIds() {
        //fetch others posts
        guard let uid = Auth.auth().currentUser?.uid else {return}
        Database.database().reference().child("following").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard let userIdsDictionary = snapshot.value as? [String: Any] else {return}
            userIdsDictionary.forEach({ (key,value) in
                Database.fetchUserWithUID(uid: key, completion: { (user) in
                    self.fetchPostWithUser(user: user)
                })
            })
        }) { (err) in
            print("failed to getch following user ids", err)
        
        }
    }
    
    var posts = [Post]()
    
    //fetch own posts
    fileprivate func fetchPosts() {
        guard let uid = Auth.auth().currentUser?.uid else {return}
       
        Database.fetchUserWithUID(uid: uid) { (user) in
            self.fetchPostWithUser(user: user)
        }
    }
        fileprivate func fetchPostWithUser(user: User) {
            
            //guard let uid = Auth.auth().currentUser?.uid else {return}
            
            let ref = Database.database().reference().child("posts").child(user.uid)
            ref.observeSingleEvent(of: .value, with: { (snapshot) in
                
                //To stop refreshing
                self.collectionView?.refreshControl?.endRefreshing()
                
                guard let dictionaries = snapshot.value as? [String: Any] else {return}
                dictionaries.forEach({ (key, value) in
                    
                    guard let dictionary = value as? [String: Any] else {return}
                    
                    var post = Post(user: user, dictionary: dictionary)
                    post.id = key
                    
                    guard let uid = Auth.auth().currentUser?.uid else {return}
                    
                    Database.database().reference().child("likes").child(key).child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                        print(snapshot)
                        
                        if let value = snapshot.value as? Int, value == 1 {
                            post.hasLiked = true
                        } else {
                            post.hasLiked = false
                        }
                        
                        self.posts.append(post)
                        self.posts.sort(by: { (p1, p2) -> Bool in
                            return p1.creationDate.compare(p2.creationDate) == .orderedDescending
                        })
                        
                        self.collectionView?.reloadData()
                    }, withCancel: { (err) in
                        print("failed to fetch like info posts", err)
                    })
                    
                })
                
            }) { (err) in
                print("Failed to fetch post", err)
            }
        }
    
    func setupNavigationItem() {
        navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "logo2"))
        
        //can't call from assets.xcassets???
        //navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "logo2").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleCamera))
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "camera3").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleCamera))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "CUR", style: .plain, target: self, action: #selector(moveCurrency))
    
    }
    
    @objc func moveCurrency() {
        let currencyController = CurrencyController()
        navigationController?.pushViewController(currencyController, animated: true)
    }
    
    @objc func handleCamera() {
        print("handling camera")
        
        let cameraController = CameraController()
        present(cameraController, animated: true, completion: nil)
        
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //give a space below postimageview
        var height: CGFloat = 40 + 8 + 8 //username userprofileimageview
        height += view.frame.width
        height += 50
        height += 60
        return CGSize(width: view.frame.width, height: height)
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! HomePostCell
        
        //solution for scrolling prblem
        if indexPath.item < posts.count {
             cell.post = posts[indexPath.item]
        }
       
        //episode 33
        cell.delegate = self
        return cell
    }
    
    func didTapComment(post: Post) {
        print("messages coming from HomeController")
        print(post.caption)
        let commentsController = CommentsController(collectionViewLayout: UICollectionViewFlowLayout())
        commentsController.post = post
        navigationController?.pushViewController(commentsController, animated: true)
    }
    
    func didLike(for cell: HomePostCell) {
        print("Handling like inside of controller")
        
        guard let indexPath =  collectionView.indexPath(for: cell) else {return}
        var post = self.posts[indexPath.item]
        print(post.caption)
        
        guard let postId = post.id else { return }
        guard let uid = Auth.auth().currentUser?.uid else {return}
        let values = [uid: post.hasLiked == true ? 0 : 1]
        Database.database().reference().child("likes").child(postId).updateChildValues(values) { (err, _) in
            if let err = err {
                print("Failed to like posts", err)
                return
            }
            
            print("Successfully liked posts")
            //in order to make like button black after tapping white button
            post.hasLiked = !post.hasLiked
            
            self.posts[indexPath.item] = post
            self.collectionView.reloadItems(at: [indexPath])
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
}
