//
//  Comment.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/07/29.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import Foundation

struct Comment {
    
    let user: User
    
    let text: String
    let uid: String
    let toId: String
    let creationDate: Date
    
    init(user: User, dictionary:[String: Any]) {
        self.user = user
        self.text = dictionary["text"] as? String ?? ""
        self.uid = dictionary["uid"] as? String ?? ""
        self.toId = dictionary["toId"] as? String ?? ""
        let secondsFrom1970 = dictionary["creationDate"] as? Double ?? 0
        self.creationDate = Date(timeIntervalSince1970: secondsFrom1970)
    }
}
