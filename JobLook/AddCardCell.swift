//
//  AddCardCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/10/19.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

class AddCardCell: UICollectionViewCell {
    
    var card: Card? {
        didSet {
            
            guard let card = card else {return}
            
            guard let cardBrand = card.brand else {return}
            guard let cardLast4 = card.last4 else {return}
            addedCardLabel.text = "\(cardBrand) ending with \(cardLast4)"
            
        }
    }
    
    let addedCardLabel: UILabel = {
        let label = UILabel()
        label.text = "You have no credit card added yet"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textAlignment = .left
        label.backgroundColor = UIColor.gray
        return label
    }()
    
//    let zipTextField: UITextField = {
//        let tf = UITextField()
//        tf.placeholder = "Zip"
//        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
//        tf.font = UIFont.systemFont(ofSize: 14)
//        tf.borderStyle = .roundedRect
//        tf.keyboardType = UIKeyboardType.numberPad
//        return tf
//    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        setupCard()
    }
    
    func setupCard() {
        
        addSubview(addedCardLabel)
        addedCardLabel.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
