//
//  CalendarViewCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/10/20.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

//import UIKit
//import FSCalendar
//
//class CalendarViewCell: FSCalendarCell {
//    
//    let dateLabel: UILabel = {
//        let label = UILabel()
//        label.text = "10:00~12:00"
//        label.textAlignment = .right
//        label.backgroundColor = .lightGray
//        label.font = UIFont.boldSystemFont(ofSize: 14)
//        return label
//        }()
//    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        
//        //backgroundColor = .blue
//        
//        addSubview(dateLabel)
//        dateLabel.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//}
