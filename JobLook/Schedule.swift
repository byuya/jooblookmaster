//
//  Schedule.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/10/12.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import Foundation

struct Schedule {
    
    let user: User
    //let uid: String
    let date: String
    let time: String
    
    init(user: User, dictionary: [String: Any]) {
        self.user = user
        //self.uid = uid
        self.date = dictionary["date"] as? String ?? ""
        self.time = dictionary["time"] as? String ?? ""
        
    }
}
