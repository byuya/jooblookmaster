//
//  UserProfileController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/07/16.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class UserProfileController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UserProfileHeaderDelegate {
    
     var ref: DatabaseReference!
     var handleRef: DatabaseHandle!
    
    let cellId = "cellId"
    let homePostCellId = "homePostCellId"
    
    var userId: String?
    
    var window: UIWindow?
    
    var isGridVIew = true
    
    func didChangeToGridView() {
        isGridVIew = true
        collectionView.reloadData()
    }
    
    func didChangeToListView() {
        isGridVIew = false
        collectionView.reloadData()
        
    }
    
    func didTapBookmark() {
        let eventForHostController = EventForHostController(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.pushViewController(eventForHostController, animated: true)
    }
    
    
    func didTapEdit() {
        print("coming from UserProfileHeader in order to editprofile")
        let editProfileController = EditProfileController(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.pushViewController(editProfileController, animated: true)
    }
    
    func didTapRegister() {
        print("coming from header register buttton")
        let hostRegisterController = HostRegisterController(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.pushViewController(hostRegisterController, animated: true)

    }
    
    func didTapSend() {
        print("handling send")
        
        let purchaseHistoryController = PurchaseHistoryController(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.pushViewController(purchaseHistoryController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.backgroundColor = .white
        //episode 25 to detect shifting username on a navigationbar
        //navigationItem.title = Auth.auth().currentUser?.uid
        
        collectionView?.register(UserProfileHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "headerId")
        
        collectionView?.register(UserProfilePhotoCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(HomePostCell.self, forCellWithReuseIdentifier: homePostCellId)
        
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        ref = Database.database().reference().child("user-comment").child(uid)
        
        setupCommentIcon()
        setupLogoutButton()
        
        fetchUser()
        updatePosts()
        
        //fetchOrderedPosts()
        
        fetchCommentTest()
        
    }
    
    func fetchCommentTest() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        let ref = Database.database().reference().child("user-comment").child(uid)
        ref.observe(.value, with: { (snapshot) in
            
            guard let dictionary = snapshot.value as? [String: Any] else {return}
            dictionary.forEach({ (key, value) in
                if key == "totalCount" {
                    
                    guard let badgeCount = value as? Int else {return}
                    if badgeCount == 0 {
                        
                        self.badgeLabel.text = nil
                        self.badgeLabel.backgroundColor = nil
                         self.tabBarController?.tabBar.items?[4].badgeValue = nil
                        
                    } else {
                        
                        let badgeString = String(badgeCount)
                        self.badgeLabel.backgroundColor = .red
                        self.badgeLabel.text = badgeString
                        self.tabBarController?.tabBar.items?[4].badgeValue = badgeString
                        
                        return
                    }
                    
                    
                }
            })
        }) { (err) in
            print("failed to fetch read coutn data", err.localizedDescription)
            return
        }
    }
    
   
    var countArray = [Int]()
    
    let commentsController = CommentsController(collectionViewLayout: UICollectionViewFlowLayout())
    
    static let updateCommentHistoryArray = NSNotification.Name(rawValue: "updateCommentHistoryArray")
    
     var commentHistories = [CommentHistory]()
    
//    func fetchingComment() {
//
//        guard let uid = Auth.auth().currentUser?.uid else {return}
//
//        handleRef = ref.observe(.childAdded, with: { (snapshot) in
////
////        let reference = Database.database().reference().child("user-comment").child(uid)
////        reference.observe(.childAdded, with: { (snapshot) in
//
//            self.commentHistories.removeAll()
//
//            if snapshot.key == "totalCount" {
//                guard let badgeCount = snapshot.value as? Int else {return}
//                let badgeString = String(badgeCount)
//                self.badgeLabel.text = badgeString
//            }
//
//            let toId = snapshot.key
//
//            //print("snapshot", snapshot.value)
//            guard let dictionary = snapshot.value as? [String: Any] else {return}
//
//            dictionary.forEach({ (key, value) in
//                print("postUser", key)
//
//                guard let commentDictionary = value as? [String: Any] else {return}
//                commentDictionary.forEach({ (postIdKey, postIdValue) in
//
//                    print("this is postUser", postIdValue)
//
//                    guard let commentDict = postIdValue as? [String: Any] else {return}
//
//                    Database.fetchUserWithUID(uid: uid, completion: { (user) in
//                        Database.fetchPostWithId(postUser: key, postId: postIdKey, completion: { (testPost) in
//                            var commentHistory = CommentHistory(user: user, testPost: testPost, dictionary: commentDict)
//                            commentHistory.id = postIdKey
//                            commentHistory.toId = toId
//                            commentHistory.postUser = key
//
//                            self.commentHistories.append(commentHistory)
////                            NotificationCenter.default.post(name: UserProfileController.updateCommentHistoryArray, object: nil, userInfo: ["commentHistories": self.commentHistories])
//
//                            self.collectionView.reloadData()
//                        })
//                    })
//                })
//
//            })
//        }) { (err) in
//            print("failed to fetch data", err.localizedDescription)
//            return
//        }
//    }
    
    let badgeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 0, alpha: 0.5)
        label.frame = CGRect(x: 10, y: -10, width: 20, height: 20)
        //label.backgroundColor = .red
        label.font = UIFont.boldSystemFont(ofSize: 12)
        //label.text = "1"
        label.textAlignment = .center
        label.textColor = .white
        label.layer.cornerRadius = label.bounds.size.height / 2
        label.layer.masksToBounds = true
        return label
    }()
    
    lazy var rightBarButton: UIButton = {
        let button = UIButton()
        button.frame = CGRect(x: 10, y: -10, width: 20, height: 20)
        button.setBackgroundImage(#imageLiteral(resourceName: "comment"), for: .normal)
        button.addTarget(self, action: #selector(handleComment), for: .touchUpInside)
        return button
        
    }()
    
    @objc func handleComment() {
        print("tapping comment icon")
        
        
        let checkCommentController = CheckCommentController(collectionViewLayout: UICollectionViewFlowLayout())
        //checkCommentController.commentHistories = commentHistories
        navigationController?.pushViewController(checkCommentController, animated: true)
        
    }
    
    private func setupCommentIcon() {
        
        //        let rightBarButton = UIButton(frame: CGRect(x: 10, y: -10, width: 20, height: 20))
        //        rightBarButton.setBackgroundImage(#imageLiteral(resourceName: "comment"), for: .normal)
        //        rightBarButton.addTarget(self, action: #selector(handleComment), for: .touchUpInside)
        
        rightBarButton.addSubview(badgeLabel)
        let leftBarButtonItem = UIBarButtonItem(customView: rightBarButton)
        navigationItem.leftBarButtonItem = leftBarButtonItem
        
    }
    
    fileprivate func setupLogoutButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "gear").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleLogOut))
    }
    
    @objc func handleLogOut() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "ログアウト", style: .destructive, handler: { (_) in
            
            do {
                try Auth.auth().signOut()
                
                let loginController = LoginController()
                let navController = UINavigationController(rootViewController: loginController)
                self.present(navController, animated: true, completion: nil)
                
            } catch let signOutErr {
                print("failed to singn out", signOutErr)
            }
            
        }))
        
        alertController.addAction(UIAlertAction(title: "キャンセル", style: .cancel, handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }
    
    var user: User?
    //ユーザーの情報を引っ張る関数
    fileprivate func fetchUser() {
        
        let uid = userId ?? (Auth.auth().currentUser?.uid ?? "")
        //guard let uid = Auth.auth().currentUser?.uid else {return}
        
        Database.fetchUserWithUID(uid: uid) { (user) in
            self.user = user
            self.navigationItem.title = self.user?.username
            
            //avoid duplication of fetching user data part1
            self.collectionView?.reloadData()
            
            self.paginatePosts()
        }
        
    }
    
    func updatePosts() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        let updateRef = Database.database().reference().child("posts").child(uid)
        updateRef.observe(.childChanged, with: { (snap) in
            
            print("did it change its node???")
            
            self.posts.removeAll()
            self.isFinishPaging = false
            
           self.paginatePosts()
        
        }) { (err) in
            print("failed to fetch posts", err.localizedDescription)
            return
        }
    }
    
    
    var isFinishPaging = false
    var posts = [Post]()
    
    fileprivate func paginatePosts() {
        print("Start paging for more posts")
        
        guard let uid = self.user?.uid else {return}
        
        let ref = Database.database().reference().child("posts").child(uid)
        
//        let value = "-LIEVuN96D0_nmXjHmQm"
//        let query = ref.queryOrderedByKey().queryStarting(atValue: value).queryLimited(toFirst: 4)
        
        //var query = ref.queryOrderedByKey()
        
        var query = ref.queryOrdered(byChild: "creationDate")
        
        if posts.count > 0 {
            //let value = posts.last?.id
            let value = posts.last?.creationDate.timeIntervalSince1970
            query = query.queryEnding(atValue: value)

        }
        
        query.queryLimited(toLast: 4).observeSingleEvent(of: .value, with: { (snapshot) in
                
        //this is count fetch each post
//            guard let dictionary = snapshot.value as? [String: Any] else {return}
//            dictionary.forEach({ (key, value) in
//
//            let countReference = Database.database().reference().child("user-comments").child(key).child(uid)
//                countReference.observe(.childAdded, with: { (snap) in
//
//                    guard let countDictionary = snap.value as? [String: Any] else {return}
//                    countDictionary.forEach({ (countKey, countValue) in
//                        if countKey == "read" {
//                            self.count = countValue as? Int
//
//                            print("this is the count", countValue)
//                        }
//                    })
//                }, withCancel: { (err) in
//                    print("failed to fetch countData", err.localizedDescription)
//                    return
//                })
//            })

            guard var allObjects = snapshot.children.allObjects as? [DataSnapshot] else {return}
            
            allObjects.reverse()
            if allObjects.count < 4 {
                self.isFinishPaging = true
            }
            
            if self.posts.count > 0 && allObjects.count > 0 {
                allObjects.removeFirst()
            }
            guard let user = self.user else {return}
            
            allObjects.forEach({ (snapshot) in
                print(snapshot.key)
                
                guard let dictionary = snapshot.value as? [String: Any] else {return}
                var post = Post(user: user, dictionary: dictionary)
                post.id = snapshot.key
                
                self.posts.append(post)
                
            })
            
            self.posts.forEach({ (post) in
                print(post.id ?? "")
            })
            
            self.collectionView?.reloadData()
            
        }) { (err) in
            print("Failed to paginate for posts:", err)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //how to fire off paginate call
        if indexPath.item == self.posts.count - 1 && !isFinishPaging {
            print("paginating posts")
            paginatePosts()
            //updatePosts()
        
        }
        if isGridVIew {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! UserProfilePhotoCell
            
            cell.post = posts[indexPath.item]
            return cell
            
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homePostCellId, for: indexPath) as! HomePostCell
            cell.post = posts[indexPath.item]
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if isGridVIew {
            let width = (view.frame.width - 2) / 3
            return CGSize(width: width, height: width + 20 + 20 + 20 + 30)
        } else {
            
            var height: CGFloat = 40 + 8 + 8 //username userprofileimageview
            height += view.frame.width
            height += 50
            height += 60
            return CGSize(width: view.frame.width, height: height)
            
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerId", for: indexPath) as! UserProfileHeader
        
        header.user = self.user
        header.delegate = self
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 200)
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let post = posts[indexPath.item]
        print(post.title)
        
        let postController = PostController(collectionViewLayout: UICollectionViewFlowLayout())
//        userProfileController.userId = user.uid
        postController.post = post
        //postController.poserId = post.id
        navigationController?.pushViewController(postController, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        tabBarController?.tabBar.isHidden = true
//    }

}

//Episode 22 (No longer need the code below due to refactoring)//
//   Database.database().reference().child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
//            print(snapshot.value ?? "")
//
//            guard let dictionary = snapshot.value as? [String: Any] else {return}
//
//            self.user = User(uid: uid, dictionary: dictionary)
//            self.navigationItem.title = self.user?.username
//
//            //avoid duplication of fetching user data part1
//            self.collectionView?.reloadData()
//
//        }) { (err) in
//            print("failed to fetch users:", err)
//        }

//    fileprivate func fetchOrderedPosts() {
//        guard let uid = self.user?.uid else { return }
//        let ref = Database.database().reference().child("posts").child(uid)
//
//        ref.queryOrdered(byChild: "creationDate").observe(.childAdded, with: { (snapshot) in
//
//            guard let dictionary = snapshot.value as? [String: Any] else {return}
//
//            guard let user = self.user else {return}
//            let post = Post(user: user, dictionary: dictionary)
//
//            self.posts.insert(post, at: 0)
//
//            self.collectionView?.reloadData()
//
//        }) { (err) in
//            print("failed to fetch ordered posts", err)
//        }
//    }
