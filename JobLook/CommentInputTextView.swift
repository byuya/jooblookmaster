//
//  CommentInputTextView.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/03.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

class CommentInputTextView: UITextView {
    
    fileprivate let placeHolderLabel: UILabel = {
        let label = UILabel()
        label.text = "Leave a comment..."
        label.textColor = UIColor.lightGray
        return label
    }()
    
    func showPlaceholderLabel() {
        placeHolderLabel.isHidden = false
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleTextChange), name: UITextView.textDidChangeNotification, object: nil)
        
        addSubview(placeHolderLabel)
        placeHolderLabel.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 8, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
    }
    
    @objc func handleTextChange() {
        placeHolderLabel.isHidden = !self.text.isEmpty
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
