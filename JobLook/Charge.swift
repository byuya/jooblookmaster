//
//  Charge.swift
//  JobLook
//
//  Created by Akiya Ozawa on 10/28/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit

struct Charge {
    
    var chargeId: String
    let user: User?
    let amount: Int
    //let accountId: String
    //let title: String
    var key: String
    init(user: User, dictionary: [String: Any]) {
        
        self.user = user
        self.chargeId = dictionary["id"] as? String ?? ""
        self.amount = dictionary["amount"] as? Int ?? 0
        self.key = dictionary["key"] as? String ?? ""
        
       // self.title = dictionary["title"] as? String ?? ""
        
    }
}
