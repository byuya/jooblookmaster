//
//  DepositHistoryCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on 12/22/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit

class DepositHistoryCell: UICollectionViewCell {
    
    var fund: Fund? {
        didSet {
            
            titleLabel.text = fund?.title
            guard let amount = fund?.amount else {return}
            amountLabel.text = "¥\(amount)"
        }
    }
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Title here"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        //label.backgroundColor = UIColor.gray
        return label
    }()
    
    let amountLabel: UILabel = {
        let label = UILabel()
        label.text = "¥1,000 "
        label.font = UIFont.boldSystemFont(ofSize: 20)
        //label.backgroundColor = UIColor.gray
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.mainBlue()
        
        setupView()
    }
    
    func setupView() {
        
        addSubview(titleLabel)
        titleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 15, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(amountLabel)
        amountLabel.anchor(top: topAnchor, left: titleLabel.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 10, height: 0, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
