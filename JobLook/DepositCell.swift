//
//  DepositCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on 12/11/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit

class DepositCell: UICollectionViewCell {
    
    var bank: Bank? {
        didSet{
            
            bankNameLabel.text = bank?.bankName
        
            bankLast4Label.text = bank?.last4
        }
    }
    let bankNameLabel: UILabel = {
        let label = UILabel()
        label.text = "OYA's Bank"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        //label.backgroundColor = UIColor.gray
        return label
    }()
    
    let bankLast4Label: UILabel = {
        let label = UILabel()
        label.text = "1234"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        //label.backgroundColor = UIColor.gray
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .blue
        
        setupView()
    }
    
    func setupView() {
        
        addSubview(bankNameLabel)
        bankNameLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 15, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(bankLast4Label)
        bankLast4Label.anchor(top: topAnchor, left: bankNameLabel.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 10, height: 0, width: 0)
        
        let seperatorView = UIView()
        seperatorView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        addSubview(seperatorView)
        seperatorView.anchor(top: bankLast4Label.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0.5, width: 0)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
