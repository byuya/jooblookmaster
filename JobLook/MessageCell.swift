//
//  File.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/06.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

class MessageCell: UICollectionViewCell {
    
    var message: Message? {
        didSet {
            
            guard let message = message else {return}
            
            messageView.text = message.messageText
            
            guard let profileImageUrl = message.user?.profileImageUrl else {return}
            profileImageView.loadImage(urlString: profileImageUrl)
            
        }
        
    }
    let messageView: UITextView = {
        let textView = UITextView()
        textView.text = "Sample text here"
        textView.font = UIFont.systemFont(ofSize: 16)
        textView.backgroundColor = UIColor.clear
        textView.textColor = .white
        textView.isEditable = false
        return textView
    }()
    
    static let blueColor = UIColor.rgb(red: 0, green: 137, blue: 249)
    
    let bubbleView: UIView = {
        let view = UIView()
        view.backgroundColor = blueColor
        view.layer.cornerRadius = 16
        view.layer.masksToBounds = true
        return view
    }()
    
    let profileImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.backgroundColor = .blue
        imageView.layer.cornerRadius = 16
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        //imageView.image = UIImage(named: "profile_selected")
        return imageView
    }()
    
    var bubbleWidthAnchor: NSLayoutConstraint?
    var bubbleRightAnchor: NSLayoutConstraint?
    var bubbleLeftAnchor: NSLayoutConstraint?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //backgroundColor = .red
    
    addSubview(bubbleView)
        bubbleView.anchor(top: topAnchor, left: nil, bottom: bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 8, height: 0, width: 200)
        bubbleWidthAnchor = bubbleView.widthAnchor.constraint(equalToConstant: 200)
        bubbleWidthAnchor?.isActive = true
        
        bubbleRightAnchor = bubbleView.rightAnchor.constraint(equalTo: rightAnchor, constant: -8)
        bubbleRightAnchor?.isActive = true
        
        bubbleLeftAnchor = bubbleView.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: 8)
        
    addSubview(messageView)
        messageView.anchor(top: topAnchor, left: bubbleView.leftAnchor, bottom: bottomAnchor, right: bubbleView.rightAnchor, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, height: 0, width: 200)
        
    addSubview(profileImageView)
        profileImageView.anchor(top: nil, left: leftAnchor, bottom: bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, height: 32, width: 32)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
