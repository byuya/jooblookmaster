//
//  PostFooter.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/31.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

class PostFooter: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let cellId = "cellId"
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor(white: 0, alpha: 0.03)
        //cv.backgroundColor = UIColor.mainBlue()
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        collectionView.register(FooterCell.self, forCellWithReuseIdentifier: cellId)
        
        addSubview(collectionView)
        collectionView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! FooterCell
        
        if indexPath.row == 0 {
             cell.priceLabel.text = "¥10,000"
            cell.purchaseButton.isHidden = true
            
        } else if indexPath.row == 1 {
            cell.priceLabel.text = nil
           
        }
    
        //cell.backgroundColor = UIColor.yellow
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width / 2, height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

protocol PostFooterCellDelegate {
    
    func didTapPurchase(for footer: FooterCell)
}

class FooterCell: UICollectionViewCell {
    
    var delegate: PostFooterCellDelegate?
    
    let priceLabel: UILabel = {
        let pl = UILabel()
        pl.text = "¥5,000"
        pl.font = UIFont.systemFont(ofSize: 20)
        pl.textColor = UIColor.white
        pl.textAlignment = .left
        return pl
    }()
    
    lazy var purchaseButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "right_arrow_shadow").withRenderingMode(.alwaysOriginal), for: .normal)
        //button.setTitle("Purchase", for: .normal)
        button.tintColor = UIColor(white: 0, alpha: 0.2)
        button.addTarget(self, action: #selector(handlePurchase), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handlePurchase(view: UIView) {
        print("Tapping handle purchase buttoon")
        delegate?.didTapPurchase(for: self)
        
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //backgroundColor = .cyan
        
        addSubview(priceLabel)
        priceLabel.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 20, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(purchaseButton)
        purchaseButton.anchor(top: topAnchor, left: nil, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 20, height: 0, width: 0)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        
    }
}
