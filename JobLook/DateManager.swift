//
//  DateManager.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/15.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

//extension DateFormatter {
//
//    enum Template
//}

extension Date {
    
    func monthAgoDate() -> Date {
        let addValue = -1
        let calender = Calendar.current
        var dateComponents = DateComponents()
        dateComponents.month = addValue
        return calender.date(byAdding: dateComponents, to: self)!
    }
    
    func monthLaterDate() -> Date {
        let addValue: Int = 1
        let calender = Calendar.current
        var dateComponents = DateComponents()
        dateComponents.month = addValue
        return calender.date(byAdding: dateComponents, to: self)!
    }
}
class DateManager: NSObject {
    
    var currentMonthsDates = [NSDate]()
    
    var now = Date()
    var selectedDate = Date()
    let daysPerWeek: Int = 7
    var numberOfItems: Int! = 0
    
    func daysAcquisition() -> Int {
        let rangeOfWeeks = Calendar.current.range(of: .weekOfMonth, in: .month, for: firstDateOfMonth() as Date)
        
        let numberOfWeeks = Int((rangeOfWeeks?.count)!)
        numberOfItems = numberOfWeeks * daysPerWeek
        return numberOfItems
    }
    
    func firstDateOfMonth() -> Date {
        
        var components = Calendar.current.dateComponents([.year, .month, .day], from: selectedDate)
        
        components.day = 1
        let firstDateMonth = Calendar.current.date(from: components)!
        return firstDateMonth
        
    }
    
    func dateForCellAtIndexPath(numberOfItem: Int) {
        
        let ordinalityOfFirstDay = Calendar.current.ordinality(of: .day, in: .weekOfMonth, for: firstDateOfMonth())
        
        for i in 0 ..< numberOfItems {
            var dateComponents = DateComponents()
            dateComponents.day = i - (ordinalityOfFirstDay! - 1)
            let date = Calendar.current.date(byAdding: dateComponents as DateComponents, to: firstDateOfMonth() as Date)!
            
            currentMonthsDates.append(date as NSDate)
        }
    }
    
    func conversionDateFormat(indexPath: IndexPath) -> String {
        
        dateForCellAtIndexPath(numberOfItem: numberOfItems)
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "dd"
        return formatter.string(from: currentMonthsDates[indexPath.row] as Date)
    }
    
    func prevMonth(date: Date) -> Date {
        currentMonthsDates = []
        selectedDate = date.monthAgoDate()
        return selectedDate
    }
    
    func nextMonth(date: Date) -> Date {
        currentMonthsDates = []
        selectedDate = date.monthLaterDate()
        return selectedDate
    }
    
    
    
}
