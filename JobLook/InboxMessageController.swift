//
//  InboxMessageController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/04.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class InboxMessageController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var ref: DatabaseReference!
    
    var handle: DatabaseHandle!
    
    var message: Message?
    let cellId = "cellId"
    var user: User?
    var userId: String?

    var totalCount: UInt?
    var readCount: UInt?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .white
        navigationItem.title = "Inbox"
        
        //navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Update", style: .plain, target: self, action: #selector(handleUpdate))
       
        //navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "new_message_icon"), style: .plain, target: self, action: #selector(handleNewMessage))
        
         //NotificationCenter.default.addObserver(self, selector: #selector(handleMessageUpdate), name: MessageController.updateMessageCount, object: nil)
        
        collectionView.alwaysBounceVertical = true
        
        collectionView.register(InboxUserCell.self, forCellWithReuseIdentifier: cellId)
        
        //fetchUserMessages()
        
        //fetchMessageUsers()
        
        //tabBarItem.badgeValue = "1"
        
        //tabBarController?.tabBar.items?[1].badgeValue = "1"
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        ref = Database.database().reference().child("user-messages").child(uid)
    }

    @objc func handleNewMessage() {
    
        let createController = CreateMessageController(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.pushViewController(createController, animated: true)
        
    }
    
    var messages = [Message]()
    var messagesDictionary = [String: Message]()
    var numberArray = [UInt]()
    var allDifference = [UInt]()
    var countArray = [UInt]()
    
    fileprivate func fetchUserMessages() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        //let userRef = Database.database().reference().child("user-messages").child(uid)
        
        //ref.child(uid).observe(.value, with: { (snapshot) in
        handle = ref.observe(.value, with: { (snapshot) in
       //self.numberArray.removeAll()
        //userRef.observe(.value, with: { (snapshot) in
            
            guard let dictionary = snapshot.value as? [String: Any] else {return}
            dictionary.forEach({ (keys, value) in
                
                if keys == "totalCount" {
                    self.totalCount = value as? UInt
                    //print("totalcount", self.totalCount)
                    
                }
                //print("let's get value", value)
                
                guard let dict = value as? [String: Any] else {return}
                guard let count = dict["read"] as? UInt else {return}
                
//                self.numberArray.append(count)
//                print("let me see this", self.numberArray)
//                let total = self.numberArray.reduce(0, {total, number in total + number})
//                print("this is count", total)
                
                //print("this is the coount", count)
                let reference = Database.database().reference().child("user-messages").child(uid).child(keys)
                reference.queryLimited(toLast: 2).observe(.value, with: { (snap) in
                    
                    guard let dictionaries = snap.value as? [String: Any] else {return}
                    dictionaries.forEach({ (key, value) in
                        //print("this shoud me messageid", key)
                        
                        let messageRef = Database.database().reference().child("messages").child(key)
                        messageRef.observeSingleEvent(of: .value, with: { (snaps) in
                            
                            if let dictionary = snaps.value as? [String: Any] {
                                Database.fetchUserWithUID(uid: keys, completion: { (user) in
                                    var message = Message(user: user, dictionary: dictionary)
                                    //message.countNumber = snap.childrenCount
                                    
                                    message.read = count
                                    
                                    if let chatPartnerId = message.chatPartnerId() {
                                        message.user = user
                                        self.messagesDictionary[chatPartnerId] = message
                                        self.messages = Array(self.messagesDictionary.values)
                                        self.messages.sort(by: { (m1, m2) -> Bool in
                                            return m1.creationDate.timeIntervalSince1970 > m2.creationDate.timeIntervalSince1970
                                        })
                                        
                                    }
                                    
                                    guard let total = self.totalCount else {return}
                                    
                                    if total == UInt(0) {
                                        //UIApplication.shared.applicationIconBadgeNumber = 0
                                        self.tabBarController?.tabBar.items?[1].badgeValue = nil
                                    } else {
                                        //UIApplication.shared.applicationIconBadgeNumber = Int(total)
                                        self.tabBarController?.tabBar.items?[1].badgeValue = "\(total)"
                                    }
                                    
                                   
                                        self.collectionView.reloadData()
                                    
                                })
                            }
                            
                        }, withCancel: { (err) in
                            print("faled to get messages")
                        })
                        
                    })
                    
                }, withCancel: { (err) in
                    print("failed to get messageid", err)
                })
            })
        }) { (err) in
            print("failed to fetch data", err)
        }
    }
    
    var timer: Timer?
    
    @objc func handleReloadTable() {
         print("we reloaded the table")
        self.collectionView.reloadData()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
        
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! InboxUserCell
        cell.message = messages[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 66)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let message = messages[indexPath.item]
        
        let messangerController = MessageController(collectionViewLayout: UICollectionViewFlowLayout())
        messangerController.userId = message.chatPartnerId()
        //messangerController.countForTotal = message.read
        navigationController?.pushViewController(messangerController, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchUserMessages()
        //fetchMessages()
        print("view showed up")
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("view did disappear here")
        //ref.removeAllObservers()
        //ref.removeObserver(withHandle: handle)
        
    }
    
//    fileprivate func fetchMessages() {
//        guard let uid = Auth.auth().currentUser?.uid else {return}
//
//        let userRef = Database.database().reference().child("user-messages").child(uid)
//        userRef.observe(.childAdded, with: { (snapshot) in
//
//            let toId = snapshot.key
//            //print("snapshots here", snapshot.value)
//
//            if snapshot.key == "totalCount" {
//                self.totalCount = snapshot.value as? UInt
//
//            }
//
//            let ref = Database.database().reference().child("user-messages").child(uid).child(toId)
//            ref.queryLimited(toLast: 2).observe(.value, with: { (snap) in
//
//                guard let dictionaries = snap.value as? [String: Any] else {return}
//                dictionaries.forEach({ (key, value) in
//
//                    if key == "read" {
//                        self.readCount = value as? UInt
//                    }
//
//                    let messageRef = Database.database().reference().child("messages").child(key)
//                    messageRef.observeSingleEvent(of: .value, with: { (snaps) in
//
//                        if let dictionary = snaps.value as? [String: Any] {
//                            Database.fetchUserWithUID(uid: toId, completion: { (user) in
//                                var message = Message(user: user, dictionary: dictionary)
//                                //message.countNumber = snap.childrenCount
//
//                                message.read = self.readCount
//
//                                if let chatPartnerId = message.chatPartnerId() {
//                                    message.user = user
//                                    self.messagesDictionary[chatPartnerId] = message
//                                    self.messages = Array(self.messagesDictionary.values)
//                                    self.messages.sort(by: { (m1, m2) -> Bool in
//                                        return m1.creationDate.timeIntervalSince1970 > m2.creationDate.timeIntervalSince1970
//                                    })
//
//                                }
//
//                                self.collectionView.reloadData()
//
//                                //ref.removeAllObservers()
//
//                                guard let total = self.totalCount else {return}
//
//                                if total == UInt(0) {
//                                    //UIApplication.shared.applicationIconBadgeNumber = 0
//                                    self.tabBarController?.tabBar.items?[1].badgeValue = nil
//                                } else {
//                                    //UIApplication.shared.applicationIconBadgeNumber = Int(total)
//                                    self.tabBarController?.tabBar.items?[1].badgeValue = "\(total)"
//                                }
//
//
//                            })
//                        }
//
//                    }, withCancel: { (err) in
//                        print("faled to get messages")
//                    })
//                })
//
//
//            }, withCancel: { (err) in
//                print("failed to fetch data 2nd", err)
//
//            })
//
//        }) { (err) in
//            print("failed to fetch data 1st", err)
//        }
//    }
    
}
