//
//  ExperienceCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on 11/3/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit

class ExperienceCell: UICollectionViewCell {
    
    var convertCurrency: Double?
    
    var post: Post? {
        didSet {
            
            
            guard let postImageUrl = post?.imageUrl else {return}
            imageView.loadImage(urlString: postImageUrl)
            
            categoryLabel.text = post?.category
            placeLabel.text = post?.place
            titleLabel.text = post?.title
            
            guard let convert = post?.convert else {return}
            print("this is convert value", convert)
            
            guard let price = post?.price else {return}
            let convertedPrice = convert * Double(price)
            priceLabel.text = "\((convertedPrice * 100).rounded() / 100)"

            //priceLabel.text = "¥\(String(price))"
            
            switch post?.currencyMark {
            case "GBP":
                markLabel.text = "€"
            case "USD":
                markLabel.text = "$"
            case "JPY":
                markLabel.text = "¥"
            default:
                markLabel.text = "¥"
            }

        }
    }

    let imageView: CustomImageView = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFill
        iv.backgroundColor = UIColor.orange
        iv.clipsToBounds = true
        return iv
        
    }()
    
    let categoryLabel: UILabel = {
        let cl = UILabel()
        cl.text = "category"
        cl.font = UIFont.systemFont(ofSize: 10)
        cl.textColor = UIColor.lightGray
        cl.textAlignment = .left
        return cl
        
    }()
    
    let placeLabel: UILabel = {
        let pl = UILabel()
        pl.text = "Shibuya"
        pl.font = UIFont.systemFont(ofSize: 10)
        pl.textColor = UIColor.lightGray
        pl.textAlignment = .left
        return pl
    }()
    
    let titleLabel: UILabel = {
        let tl = UILabel()
        tl.text = "Title"
        tl.font = UIFont.boldSystemFont(ofSize: 13)
        tl.textColor = .black
        tl.textAlignment = .left
        return tl
        
    }()
    
    let markLabel: UILabel = {
        let pl = UILabel()
        pl.text = "¥"
        pl.font = UIFont.systemFont(ofSize: 10)
        pl.textColor = .black
        //pl.textAlignment = .left
        return pl
        
    }()
    
    let priceLabel: UILabel = {
        let pl = UILabel()
        pl.text = "5,000"
        pl.font = UIFont.systemFont(ofSize: 10)
        pl.textColor = .black
        pl.textAlignment = .left
        return pl
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        setupView()
    }
    
    
    func setupView() {
        
        addSubview(imageView)
        imageView.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 120, width: 0)
        
        addSubview(categoryLabel)
        categoryLabel.anchor(top: imageView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(placeLabel)
        placeLabel.anchor(top: categoryLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(titleLabel)
        titleLabel.anchor(top: placeLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(markLabel)
        markLabel.anchor(top: titleLabel.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, paddingTop: 5, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(priceLabel)
        priceLabel.anchor(top: titleLabel.bottomAnchor, left: markLabel.rightAnchor, bottom: bottomAnchor, right: nil, paddingTop: 5, paddingLeft: 2, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
