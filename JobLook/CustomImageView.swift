//
//  CustomImageView.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/07/24.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

var imageCatch = [String: UIImage]()

class CustomImageView: UIImageView {
    
    var lastURLUsedToLoadImage: String?
    func loadImage(urlString: String) {
        
        //print("Loading image....")
        lastURLUsedToLoadImage = urlString

        self.image = nil
        
        if let catchedImage = imageCatch[urlString] {
            self.image = catchedImage
            return
        }
        guard let url = URL(string: urlString) else {return}
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            if let err = err {
                print("Failed to fatch pots", err)
                return
            }
            
            if url.absoluteString != self.lastURLUsedToLoadImage {
                return
            }
            
            // if can fetch post successfully
            guard let imageData =  data else {return}
            
            let photoImage = UIImage(data: imageData)
            
            //catch images
            imageCatch[url.absoluteString] = photoImage
            
            DispatchQueue.main.async {
                self.image = photoImage
            }
            
            }.resume()
    }
}
