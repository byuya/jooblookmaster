//
//  PickerUtils.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/14.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

//extension TimeSelectorController: UIPickerViewDataSource, UIPickerViewDelegate {
//
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 1
//    }
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        return 1
//    }
//
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return timeSelectorCell.textView.text
//    }
//    
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        timeSelectorCell.textView.text = timeSelectorCell.handleSelectedTime()
//    }
//}

extension SharePhotoController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch pickerView.tag {
        case 1:
            return categories.count
        case 2:
            return places.count
        case 3:
            return languages.count
        default:
            return participants.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch pickerView.tag {
        case 1:
            return categories[row]
        case 2:
            return places[row]
        case 3:
            return languages[row]
        default:
            let person = "人"
            return String(participants[row]) + person
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 1:
            categoryView.text = categories[row]
            self.view.endEditing(true)
            print(categories[row])
        case 2:
            placeView.text = places[row]
            self.view.endEditing(true)
            print(places[row])
        case 3:
            languageView.text = languages[row]
            self.view.endEditing(true)
            print(languages[row])
        default:
            
           participantsView.text = String(participants[row]) + "人"
           
            self.view.endEditing(true)
            //print(String(participants[row]))

        }

    }
}

