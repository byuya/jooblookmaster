//
//  Card.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/10/20.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import Foundation

struct Card {
    
    let user: User
    let post: Post
    let last4: String?
    let cardId: String?
    let brand: String?
    
    init(user: User, post: Post, dictionary: [String: Any]) {
        self.user = user
        self.post = post
        self.last4 = dictionary["last4"] as? String ?? ""
        self.cardId = dictionary["id"] as? String ?? ""
        self.brand = dictionary["brand"] as? String ?? ""
    }
    
}
