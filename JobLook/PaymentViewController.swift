//
//  PaymentViewController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/10/18.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class PaymentViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, PaymentViewCellDelegate {
    
    let cellId = "cellId"
    
    var post: Post?
    var experiment: Experiment?
    
    //var card: Card?
    
    var postId: String?
    var userId: String?
    var getChargeId: String?
    var defaultCardId: String?
    
    var cardBrand: String?
    var cardLast4: String?
    var cardIds: String?
    
    var randomKey: String?
    
    let cardLabel: UILabel = {
        let label = UILabel()
        label.text = "card_xxxxxxxxx"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textAlignment = .right
        label.backgroundColor = UIColor.lightGray
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(handleHeaderAddCard), name: AddCardController.addingHeaderCardNotificationName, object: nil)
        
        self.cardLabel.text = "No card added"
        
        //collectionView.backgroundColor = UIColor.lightGray
        collectionView.backgroundColor = UIColor.rgb(red: 240, green: 240, blue: 240)
        collectionView.register(PaymentViewCell.self, forCellWithReuseIdentifier: cellId)
        
        fetchCard()
        setupView()
        
        print("postUser", post?.user.uid ?? "")
        
        randomKey = randomString(length: 10)
        
        print("this is randomkey", randomKey ?? "")
        
        //testFetch()
        
        //fetchExperimentData()
    }
    
    @objc fileprivate func handleHeaderAddCard(notification: Notification) {
        print("notification center from addcardHeader")
        
        if let userInfo = notification.userInfo {
            let id = userInfo["id"] as! String
            let brand = userInfo["brand"] as! String
            let last4 = userInfo["last4"] as! String
            
            let cardIdAfter = id
            let cardBrandAfter = brand
            let cardLast4After = last4
            
            print(cardIdAfter, cardBrandAfter, cardLast4After)
            
            cardLabel.text = "Use this card \(cardBrandAfter)\(cardLast4After)"
            
            defaultCardId = cardIdAfter
            print(defaultCardId ?? "")
            
        }
    }
    
    @objc fileprivate func handleAddingCard() {
        fetchCard()
    }
    
    @objc fileprivate func handleFeedBack() {
        print(123)
        
        let feedbackContoroller = FeedbackController(collectionViewLayout: UICollectionViewFlowLayout())
        feedbackContoroller.post = post
        navigationController?.pushViewController(feedbackContoroller, animated: true)
    }
    
    fileprivate func setupView() {
        collectionView.addSubview(cardLabel)
        cardLabel.anchor(top: collectionView.topAnchor, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 250, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: view.frame.width)
    }
    
    fileprivate func fetchCard() {
        guard let post = self.post else {return}
        guard let uid = Auth.auth().currentUser?.uid else {return}
        let ref = Database.database().reference().child("users").child(uid).child("sources")
        ref.observeSingleEvent(of: .childAdded, with: { (snapshot) in
            
            //print(snapshot.value)
            
            guard let dictionary = snapshot.value else {return}
            Database.fetchUserWithUID(uid: uid, completion: { (user) in
                let card = Card(user: user, post: post, dictionary: dictionary as! [String : Any])
                
                guard let cardBrand = card.brand else {return}
                guard let cardLast4 = card.last4 else {return}
                guard let cardId = card.cardId else {return}
                
                self.defaultCardId = cardId
                                
                self.cardLabel.text = "\(cardBrand) ending with \(cardLast4)"
                
                //self.cards.append(card)
                
                self.collectionView.reloadData()
                
            })
        }) { (err) in
            print("faled to observe source data")
        }
        
    }
    
    var posts = [Post]()
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PaymentViewCell
        cell.delegate = self
        cell.post = self.post
        //cell.addCardButton.setTitle("cardid", for: .normal)
        //cell.card = cards[indexPath.item]
        return cell
    }
    
    func didTapAddPayment() {
        print("coming from paymentviewcell")
        let addPaymentController = AddCardController(collectionViewLayout: UICollectionViewFlowLayout())
        addPaymentController.post = post
        navigationController?.present(addPaymentController, animated: true, completion: nil)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return cards.count
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 700)
    }
    
    func didTapBack() {
        print("backing to home")
        
        startLoadingAnimation()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
            self.stopLoadingAnimation()
          
            UIView.animate(withDuration: 1.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.collectionView.backgroundColor?.withAlphaComponent(0.5)
                self.popupViewWindow()
            }, completion: nil)
            
//            UIView.animate(withDuration: 0.5, animations: {
//                self.collectionView.backgroundColor?.withAlphaComponent(0.5)
//                self.popupViewWindow()
//            })
            
        }
        
        
        //navigationController?.popViewController(animated: true)
//        let indoxBoxMessageController = InboxMessageController(collectionViewLayout: UICollectionViewFlowLayout())
//        let navController = UINavigationController(rootViewController: indoxBoxMessageController)
//        present(navController, animated: true, completion: nil)
        
        //navigationController?.popToViewController(indoxBoxMessageController, animated: true)
        //navigationController?.popToViewController(navigationController!.viewControllers[0], animated: true)
    }
    
    var indicatorActivity: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func startLoadingAnimation() {
       
        collectionView.addSubview(indicatorActivity)
        indicatorActivity.anchor(top: collectionView.topAnchor, left: collectionView.leftAnchor, bottom: collectionView.bottomAnchor, right: collectionView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        indicatorActivity.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor).isActive = true
        indicatorActivity.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor).isActive = true
        
        indicatorActivity.hidesWhenStopped = true
        indicatorActivity.style = UIActivityIndicatorView.Style.whiteLarge
        
        indicatorActivity.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopLoadingAnimation() {
        indicatorActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func popupViewWindow() {
        
       let popupView = UIView()
        
        popupView.fadeIn(type: .Normal)
        
        popupView.backgroundColor = UIColor.mainBlue()
        
        let thanksLabel: UILabel = {
            let label = UILabel()
            label.text = "Thank you! \nHave a blast experience! \nLet's start chatting with the host"
            label.font = UIFont.boldSystemFont(ofSize: 15)
            label.textColor = .white
            label.numberOfLines = 0
            label.textAlignment = .center
            return label
        }()
        
        let okButton: UIButton = {
            let button = UIButton()
            button.setTitle("Ok", for: .normal)
            button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
            return button
        }()
        
        collectionView.addSubview(popupView)
        popupView.anchor(top: nil, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 80, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 180, width: 100)
        popupView.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor).isActive = true
        popupView.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor).isActive = true
        popupView.layer.masksToBounds = true
        popupView.layer.cornerRadius = 10
        
        popupView.addSubview(thanksLabel)
        thanksLabel.anchor(top: popupView.topAnchor, left: popupView.leftAnchor, bottom: nil, right: popupView.rightAnchor, paddingTop: 30, paddingLeft: 10, paddingBottom: 0, paddingRight: 10, height: 0, width: 0)
        
        let seperatorView2 = UIView()
        seperatorView2.backgroundColor = UIColor.white
        popupView.addSubview(seperatorView2)
        seperatorView2.anchor(top: nil, left: popupView.leftAnchor, bottom: popupView.bottomAnchor, right: popupView.rightAnchor, paddingTop: 0, paddingLeft: 10, paddingBottom: 50, paddingRight: 10, height: 1.0, width: 0)
        
        popupView.addSubview(okButton)
        okButton.anchor(top: nil, left: popupView.leftAnchor, bottom: popupView.bottomAnchor, right: popupView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 15, paddingRight: 0, height: 0, width: 0)
        okButton.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true
        
    }
    
    @objc fileprivate func handleBack() {
        print("back to rootview")
        self.navigationController?.popToRootViewController(animated: true)
    
    }
    
    func didTapPurchase(for amountCharge: String, for cardId: String) {
        print("coming from purchas button", amountCharge, cardId)
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let hostId = post?.user.uid else {return}
        guard let amount = post?.price else {return}
        guard let title = post?.title else {return}
        guard let caption = post?.caption else {return}
        guard let place = post?.place else {return}
        guard let accountId = post?.accountId else {return}
        //guard let card = self.cardLabel.text else {return}
        guard let cardId = defaultCardId else {return}
        guard let key = randomKey else {return}
        
        print("uid:", uid, "hostId:", hostId, "cardId:", cardId, "accountId:", accountId, "amount:", amount, "hostId:", hostId, "title:", title, "caption:", caption)
        
        let value = ["uid": uid, "hostId": hostId, "source": cardId, "amount": amount, "key": key, "accountId": accountId, "title": title, "caption": caption, "place": place] as [String: Any]
        
        let ref = Database.database().reference().child("experiment")
        ref.childByAutoId().updateChildValues(value) { (err, ref) in
            if let err = err {
                print("failed to inserted charge into db", err)
            }
            
            print("successfully insered charge into db")
            
            self.startLoadingAnimation()

            //delay calling the function because i want to get chargeId written after writing into database
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: {
                //self.fetchExperimentDataAndInsert()
                self.stopLoadingAnimation()
                
                UIView.animate(withDuration: 0.5, animations: {
                    self.collectionView.backgroundColor?.withAlphaComponent(0.5)
                    self.popupViewWindow()
                })
                //self.didTapMessage()
            })
            
        }
        
        
    }
    
    func fetchExperimentDataAndInsert() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let hostId = post?.user.uid else {return}
        
        print("now fetching", uid, hostId)
        
        let ref = Database.database().reference().child("experiment").child(uid).child(hostId)
        ref.queryLimited(toLast: 1).observeSingleEvent(of: .childAdded, with: { (snapshot) in
            
            guard let dictionary = snapshot.value as? [String: Any] else {return}
            Database.fetchUserWithUID(uid: uid, completion: { (user) in
                let experiment = Experiment(user: user, dictionary: dictionary)
                
                print("amount: ", experiment.amount)
                print("chargeId: ", experiment.chargeId)
                print("balancetransaction: ", experiment.balanceTransaction)
                print("status:", experiment.status)
                print("created: ", experiment.created)
                
                let value = ["amount": experiment.amount, "chargeId": experiment.chargeId, "balance_transaction": experiment.balanceTransaction, "status": experiment.status] as [String : Any]
                let infoRef = Database.database().reference().child("users").child(uid).child("charges")
                infoRef.childByAutoId().updateChildValues(value, withCompletionBlock: { (err, ref) in
                    if let err = err {
                        print("failed to insert data", err)
                        return
                    }
                    
                    print("Successfully inserted data")
                    
                    self.stopLoadingAnimation()
                    
                    UIView.animate(withDuration: 0.5, animations: {
                        self.collectionView.backgroundColor?.withAlphaComponent(0.5)
                        self.popupViewWindow()
                    })
                    
                })
                
            })
            
        }) { (err) in
            print("failed to fetch data", err)
        }
    }
    
//////this is previous charge function to charge card
//    func didTapPurchase(for amountCharge: String, for cardId: String) {
//        print("coming from purchas button", amountCharge, cardId)
//
//        guard let amount = post?.price else {return}
//        guard let accountId = post?.accountId else {return}
//        //guard let card = self.cardLabel.text else {return}
//        guard let cardId = defaultCardId else {return}
//        guard let key = randomKey else {return}
//
//        print(cardId)
//        print(accountId)
//        print(amount)
//
//        //print("this is randomKey", randomKey)
//
//        guard let uid = Auth.auth().currentUser?.uid else {return}
//        let value = ["source": cardId, "amount": amount, "key": key] as [String: Any]
//        let ref = Database.database().reference().child("users").child(uid).child("charges")
//        ref.childByAutoId().updateChildValues(value) { (err, ref) in
//            if let err = err {
//                print("failed to inserted charge into db", err)
//            }
//
//            print("successfully insered charge into db")
//
//            self.startLoadingAnimation()
//
//            //delay calling the function because i want to get chargeId written after writing into database
//            DispatchQueue.main.asyncAfter(deadline: .now() + 6.0, execute: {
//                self.testFetch()
//
//            })
//
//        }
//
//    }
    
    func testFetch() {
        
        //print("fetch starts HERE!", randomKey)
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        let fetchChargeRef = Database.database().reference().child("users").child(uid).child("charges")
        //fetchChargeRef.queryLimited(toLast: 1).observeSingleEvent(of: .childAdded, with: { (snapshot) in
        fetchChargeRef.queryLimited(toLast: 1).observeSingleEvent(of: .childAdded, with: { (snapshot) in
            
            guard let dictionary = snapshot.value as? [String: Any] else {return}
            
            dictionary.forEach({ (key, value) in
                //print("this is key", key)
                if key == "metadata" {
                    //print("this is value for metadata", value)
                    
                    guard let dictionaries = value as? [String: Any] else {return}
                    guard let keyId = dictionaries["key"] as? String else {return}
                    
                    print("let me see what this keyid value", keyId)
                    
                    //"QD0oZTXWAp"
                    if keyId != self.randomKey {
                        print("the keyid doesn't match")
                        return
                    } else {
                        print("keyid is the same in ")
                    }
                    
                    Database.fetchUserWithUID(uid: uid, completion: { (user) in
                        var charge = Charge(user: user, dictionary: dictionary)
                        charge.key = keyId
                        
                        let creationDate = Date().timeIntervalSince1970
                        let expireDate = Date().addingTimeInterval(100).timeIntervalSince1970
                        
                        guard let accountId = self.post?.accountId else {return}
                        guard let postUser = self.post?.user.uid else {return}
                        guard let price = self.post?.price else {return}
                        let id = charge.chargeId
                        guard let guestName = charge.user?.username else {return}
                        
                        let value = ["title": self.post?.title ?? "", "price": price, "accountId": accountId, "chargeId": id, "guestName": guestName, "creationDate": creationDate, "expireDate": expireDate, "cancelRequest": 0, "feedBack": 0] as [String : Any]
                        
                        let experimentRef = Database.database().reference().child("experiment").child(uid).child(postUser)
                        experimentRef.childByAutoId().updateChildValues(value, withCompletionBlock: { (err, ref) in
                            if let err = err {
                                print("failed to store experience data", err)
                            }
                            
                            print("successfully stored experience data")
                            self.stopLoadingAnimation()
                            
                            UIView.animate(withDuration: 0.5, animations: {
                                self.collectionView.backgroundColor?.withAlphaComponent(0.5)
                                self.popupViewWindow()
                            })
                        })
                    })
                    
                }
            })
        }) { (err) in
            print("failed to fetch data", err)
        }
        
    }
    
    fileprivate func fetchChargeData() {
        
        print("fetch starts HERE!")
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        let fetchChargeRef = Database.database().reference().child("users").child(uid).child("charges")
        fetchChargeRef.queryLimited(toLast: 1).observeSingleEvent(of: .childAdded, with: { (snapshot) in
            
            //print("this is the charge data in order to get chargeid", snapshot.value)
            
            guard let dictionary = snapshot.value as? [String: Any] else {return}
            
            Database.fetchUserWithUID(uid: uid, completion: { (user) in
                
                let charge = Charge(user: user, dictionary: dictionary)
                //self.getChargeId = charge.chargeId
                
                print("chargeId", charge.chargeId)
                print(charge.user?.username ?? "")
                
                let creationDate = Date().timeIntervalSince1970
                let expireDate = Date().addingTimeInterval(100).timeIntervalSince1970
                
                guard let accountId = self.post?.accountId else {return}
                guard let postUser = self.post?.user.uid else {return}
                guard let price = self.post?.price else {return}
                 let id = charge.chargeId 
                guard let guestName = charge.user?.username else {return}
                
                let value = ["title": self.post?.title ?? "", "price": price, "accountId": accountId, "chargeId": id, "guestName": guestName, "creationDate": creationDate, "expireDate": expireDate, "cancelRequest": 0, "feedBack": 0] as [String : Any]
                
                let experimentRef = Database.database().reference().child("experiment").child(uid).child(postUser)
                experimentRef.childByAutoId().updateChildValues(value, withCompletionBlock: { (err, ref) in
                    if let err = err {
                        print("failed to store experience data", err)
                    }
                    
                    print("successfully stored experience data")
                    self.stopLoadingAnimation()
                    
                    UIView.animate(withDuration: 0.5, animations: {
                        self.collectionView.backgroundColor?.withAlphaComponent(0.5)
                        self.popupViewWindow()
                    })
                })
                
            })
            
        }) { (err) in
            
            print("failed to fetch charge data", err)
            
        }
    }
    
//    func didTapMessage() {
//        print("handling message")
//        guard let guestId = Auth.auth().currentUser?.uid else {return}
//        guard let hostId = post?.user.uid else {return}
//        let createDate = Date().timeIntervalSince1970
//        print(hostId, guestId, createDate)
//
//        let message = "Hi, Thanks for your interest!"
//        let values = ["messageText": message, "creationDate": createDate, "fromId": hostId, "toId": guestId] as [String : Any]
//
//        let ref = Database.database().reference().child("messages")
//         let childRef = ref.childByAutoId()
//        childRef.updateChildValues(values) { (err, ref) in
//
//            print(values)
//            if err != nil {
//                return
//            }
//
//            let userMessageRef = Database.database().reference().child("user-messages").child(hostId).child(guestId)
//            guard let messageId = childRef.key else {return}
//            let senderValue = [messageId: 1, "read": 0]
//            userMessageRef.updateChildValues(senderValue, withCompletionBlock: { (err, ref) in
//                if let err = err {
//                    print("failed to insert data to db", err)
//                    return
//                }
//
//                print("successfully insered messageid")
//                 let receiverValue = [messageId: 1, "read": 1]
//                let recidipentUerMessagesRef = Database.database().reference().child("user-messages").child(guestId).child(hostId)
//                recidipentUerMessagesRef.updateChildValues(receiverValue, withCompletionBlock: { (err, ref) in
//                    if let err = err {
//                        print("failed to insert recipientid", err)
//                        return
//                    }
//
//                    print("successfully insered recipentid")
//
//                    self.navigationController?.popToRootViewController(animated: true)
//
//                })
//            })
//
//        }
//    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
        
    }
    
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }
}
