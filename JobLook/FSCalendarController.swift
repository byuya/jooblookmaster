//
//  FSCalendar.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/10/10.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FSCalendar
import CalculateCalendarLogic

class FSCalenderViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance, FSCalendarHeaderDelegate {
    
    let cellId = "cellId"
    let headerId = "headerId"
    
    var schedule: Schedule?
    
    var someday: Array = [String]()
    var datesWithEvent = ["2018-10-03", "2018-10-06", "2018-10-12", "2018-10-25"]
    
    let fsCalender: FSCalendar = {
        let calendar = FSCalendar()
        return calendar
    }()
    
    let dateView: UITextView = {
        let tv = UITextView()
        //tv.text = "2018/10/11"
        tv.backgroundColor = .lightGray
        tv.isEditable = false
        return tv
    }()
    //set main schedule
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Schedule"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.backgroundColor = UIColor.gray
        return label
    }()
    
    let scheduleLabel: UILabel = {
        let label = UILabel()
        label.text = "8:00~9:00 I will do this"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.backgroundColor = UIColor.gray
        return label
    }()
    
//    let addButton: UIButton = {
//        let button = UIButton(type: .system)
//        //button.setImage(#imageLiteral(resourceName: "plus_photo").withRenderingMode(.alwaysOriginal), for: .normal)
//        button.setTitle("+", for: .normal)
//        button.layer.masksToBounds = true
//        button.layer.cornerRadius = 80/2
//        button.backgroundColor = UIColor.orange
//        button.addTarget(self, action: #selector(handleAddButton), for: .touchUpInside)
//        return button
//    }()
//
//    @objc fileprivate func handleAddButton() {
//        print("handling add buton")
//
//        let scheduleViewController = ScheduleViewController(collectionViewLayout: UICollectionViewFlowLayout())
//        //navigationController?.pushViewController(scheduleViewController, animated: true)
//        scheduleViewController.dateLabel.text = dateView.text
//        present(scheduleViewController, animated: true, completion: nil)
//    }
    
    //var schedule = [Schedule]()
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        //let selectDay = getDay(date)
        
        let tmpDate = Calendar(identifier: .gregorian)
        let year = tmpDate.component(.year, from: date)
        let month = tmpDate.component(.month, from: date)
        let day = tmpDate.component(.day, from: date)
        let hour = tmpDate.component(.hour, from: date)
        let minute = tmpDate.component(.minute, from: date)
        let m = String(format: "%02d", month)
        let d = String(format: "%02d", day)
        let h = String(format: "%02d", hour)
        let mi = String(format: "%02d", minute)
        
        titleLabel.text = "Schedule List"
        titleLabel.backgroundColor = UIColor.orange
        
        scheduleLabel.text = "NO Schudule"
        scheduleLabel.backgroundColor = UIColor.magenta
        
        let da = "\(year)/\(m)/\(d) \(h):\(mi)"
        print(da)
        //dateView.text = "\(selectDay.0)/\(selectDay.1)/\(selectDay.2)"
         // timeView.text = nil
        dateView.text = "\(year)/\(m)/\(d)"
        //print(selectDay)
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        let scheduleRef = Database.database().reference().child("schedule").child(uid)
        scheduleRef.observe(.childAdded, with: { (snapshot) in
            
            //print(snapshot.value)
            guard let dictionary = snapshot.value as? [String: Any] else {return}
            Database.fetchUserWithUID(uid: uid, completion: { (user) in
                
               let schedule = Schedule(user: user, dictionary: dictionary)
                  //print(schedule.date)
                let scheduleDate = schedule.date
                if scheduleDate == da {
                    self.scheduleLabel.text = "I will do this BABY!"
                    print("its the same date")
                }
                //if date in db matches current date, display
                
            })
        }) { (err) in
            print("failed to fetch data")
        }
        
    }
    
    var schedules = [Schedule]()
    
    func setupFetching() {
        
        let date = Date()
        
        let tmpDate = Calendar(identifier: .gregorian)
        let year = tmpDate.component(.year, from: date)
        let month = tmpDate.component(.month, from: date)
        let day = tmpDate.component(.day, from: date)
        //let hour = tmpDate.component(.hour, from: date)
        //let minute = tmpDate.component(.minute, from: date)
        let y = String(format: "%02d", year)
        let m = String(format: "%02d", month)
        let d = String(format: "%02d", day)
        //let h = String(format: "%02d", hour)
        //let mi = String(format: "%02d", minute)
        
        if let uid = Auth.auth().currentUser?.uid {
            let ref = Database.database().reference().child("schedule").child(uid).child(y).child(m).child(d)
            ref.observe(.childAdded, with: { (snapshot) in
                //print(snapshot.value)
                
                guard let dictionary = snapshot.value as? [String: Any] else {return}
                
                Database.fetchUserWithUID(uid: uid, completion: { (user) in
                    let schedule = Schedule(user: user, dictionary: dictionary)
                    
                    let scheduleDate = schedule.time
                    print(scheduleDate)
                    
                        self.schedules.append(schedule)
                        self.collectionView.reloadData()
                    
                    
                })
                
            }) { (err) in
                print("failed to fetch data")
                
            }
           
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.alwaysBounceVertical = true
        //timeView.text = nil
        
         navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(handleTimeSave))
        
        fsCalender.delegate = self
        fsCalender.dataSource = self
        
        collectionView.backgroundColor = .cyan
        
        collectionView.register(FSCalenderCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(FSCalendarHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerId)
        //setupCalendar()
        
        setupFetching()
    }
    
//    func toolBarSetup() {
//        print(123)
//        let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(handleDone))
//        timeView.inputAccessoryView = toolBar
//    }
//
//    @objc func handleDone() {
//         view.endEditing(true)
//    }
    
    var header: FSCalendarHeader?
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! FSCalenderCell
        //cell.backgroundColor = .blue
        cell.schedule = schedules[indexPath.item]
        //cell.scheduleLabel.text = dateView.text
//        collectionView.addSubview(cell)
//        cell.anchor(top: titleLabel.bottomAnchor, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width, height: 50)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return schedules.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as! FSCalendarHeader
        header.delegate = self
        return header
    }
    
    func didSelect(for scheduleHeader: String) {
        dateView.text = scheduleHeader
        print("Coming from FsCalendarHeader", scheduleHeader)
        collectionView.reloadData()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 300, height: 360)
    }
    
    
    @objc func handleTimeSave() {
        print("handling save time")
        let scheduleViewController = ScheduleViewController(collectionViewLayout: UICollectionViewFlowLayout())
        //navigationController?.pushViewController(scheduleViewController, animated: true)
        //scheduleViewController.dateLabel.text = dateView.text
        present(scheduleViewController, animated: true, completion: nil)
    }
    
    func setupCalendar() {
//        collectionView.addSubview(fsCalender)
//        fsCalender.anchor(top: collectionView.topAnchor, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 300, width: 360)
//
        collectionView.addSubview(dateView)
        dateView.anchor(top: collectionView.topAnchor, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 300, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
//
//        collectionView.addSubview(titleLabel)
//        titleLabel.anchor(top: dateView.bottomAnchor, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 360)
        
        
//        collectionView.addSubview(scheduleLabel)
//        scheduleLabel.anchor(top: titleLabel.bottomAnchor, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 80, width: 360)
        
//        collectionView.addSubview(addButton)
//        addButton.anchor(top: scheduleLabel.bottomAnchor, left: nil, bottom: nil, right: collectionView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 80, width: 80)
        
//
//        collectionView.addSubview(timePicker)
//        timePicker.anchor(top: textView.bottomAnchor, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 100, width: 360)
//        collectionView.addSubview(timeView)
//        timeView.anchor(top: timePicker.bottomAnchor, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 360)
    }
}
