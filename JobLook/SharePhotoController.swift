//
//  SharePhotoController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/07/23.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class SharePhotoController: UIViewController, UITextViewDelegate, TimeSelectorControllerDelegate {
    
    var categories = ["エンタメ", "アニメ", "グルメ", "マリーンアクティビティ"]
    var places = ["渋谷", "新宿", "秋葉原"]
    var languages = ["英語", "日本語", "中国語", "スペイン語", "韓国語"]
    
    var participants: [Int] = ([Int])(1...30)
    
    let picker = SelectNumberPicker()
    
    //var timeSelectorController: TimeSelectorController?
    //let timeSelectorController = TimeSelectorController()

    var selectedImage: UIImage? {
        didSet {
            self.imageView.image = selectedImage
        
        }
    }
    
    
    override func viewDidLoad() {
        
        view.backgroundColor = UIColor.rgb(red: 240, green: 240, blue: 240)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "投稿", style: .plain, target: self, action: #selector(handleShare))
    
        //picker.delegate = self
        //picker.dataSource = self
        //picker.isHidden = true
        
        picker.categoryPickerView.delegate = self
        picker.placePickerView.delegate = self
        picker.languagePickerView.delegate = self
        picker.participanstPickerView.delegate = self
        
       //categoryView.inputView = picker.categoryPickerView
       //placeView.inputView = picker.placePickerView
        
        categoryView.inputView = picker.categoryPickerView
        placeView.inputView = picker.placePickerView
        languageView.inputView = picker.languagePickerView
        participantsView.inputView = picker.participanstPickerView
        
        //categoryView.delegate = self
        //placeView.delegate = self
        
        //setupProfitTextView()
        setupImageAndTextViews()
        
        //timeSelectorController.delegate = self
        //fetchAccountIdBefore()
        fetchAccountIdAfter()
    }
    
    fileprivate func fetchAccountIdBefore() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        let ref = Database.database().reference().child("users").child(uid).child("host")
        ref.observeSingleEvent(of: .childAdded, with: { (snapshot) in
            
            
            guard let dictionary = snapshot.value as? [String: Any] else {return}
            Database.fetchUserWithUID(uid: uid, completion: { (user) in
                let host = Host(user: user, dictionary: dictionary)
                
                guard let hostId = host.id else {return}
               
                self.accountIdView.text = hostId
            })
            
            
        }) { (err) in
            print("failed to fetch accountid", err)
        }
    }
    
    fileprivate func fetchAccountIdAfter() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        let accountRef = Database.database().reference().child("users").child(uid).child("host")
        accountRef.observeSingleEvent(of: .value, with: { (snapshot) in
            //print(snapshot.value)
            
            guard let dictionary = snapshot.value as? [String: Any] else {return}
            Database.fetchUserWithUID(uid: uid, completion: { (user) in
                let host = Host(user: user, dictionary: dictionary)
                
                guard let hostId = host.id else {return}
                
                self.accountIdView.text = hostId
            })
        }) { (err) in
            print("failed to fetch accountId", err)
        }
    }
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .red
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    let textView: UITextView = {
        let tv = UITextView()
        //tv.text = "サービス内容を記載ください"
        tv.font = UIFont.systemFont(ofSize: 14)
        return tv
    }()
    
    let titleView: UITextField = {
        let tv = UITextField()
        tv.placeholder = "タイトル"
        tv.textAlignment = .right
        tv.backgroundColor = .white
        tv.font = UIFont.systemFont(ofSize: 16)
        tv.textColor = UIColor.lightGray
        return tv
    }()
    
    let categoryView: UITextView = {
        let cv = UITextView()
        cv.text = "ガイドカテゴリーを選んでください"
        cv.font = UIFont.systemFont(ofSize: 16)
        cv.textAlignment = .right
        cv.textColor = UIColor.lightGray
        cv.isEditable = false
        return cv
    }()
    
    let placeView: UITextView = {
        let pv = UITextView()
        pv.text = "ガイド場所を選んでください"
        pv.font = UIFont.systemFont(ofSize: 16)
        pv.textAlignment = .right
        pv.textColor = UIColor.lightGray
        pv.isEditable = false
        return pv
    }()
    
    let languageView: UITextView = {
        let lv = UITextView()
        lv.text = "ガイド言語を選んでください"
        lv.font = UIFont.systemFont(ofSize: 16)
        lv.textAlignment = .right
        lv.textColor = UIColor.lightGray
        //lv.addGestureRecognizer = true
        lv.isEditable = false
        return lv
    }()
    
    let participantsView: UITextView = {
        let partView = UITextView()
        partView.text = "一回の最大ガイド可能人数をお選びください"
        partView.font = UIFont.systemFont(ofSize: 16)
        partView.textColor = UIColor.lightGray
        partView.textAlignment = .right
        partView.isEditable = false
        return partView
    }()
    
    let accountIdView: UITextView = {
        let partView = UITextView()
        partView.text = "accountId"
        partView.font = UIFont.systemFont(ofSize: 16)
        partView.textColor = UIColor.lightGray
        partView.textAlignment = .right
        partView.isEditable = false
        return partView
    }()
    
    let scheduleButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("スケジュール設定", for: .normal)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        button.titleLabel?.textAlignment = .right
        button.addTarget(self, action: #selector(handleSchedule), for: .touchUpInside)
        button.backgroundColor = .white
        return button

    }()
    
    @objc fileprivate func handleSchedule() {
        
//        let calendarViewController = CalendarViewController(collectionViewLayout: UICollectionViewFlowLayout())
//        navigationController?.pushViewController(calendarViewController, animated: true)
//
        let fscalenarViewController = FSCalenderViewController(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.pushViewController(fscalenarViewController, animated: true)
        
//        let calenderViewController = CalenderViewController(collectionViewLayout: UICollectionViewFlowLayout())
//        navigationController?.pushViewController(calenderViewController, animated: true)
        
    }
    

    let priceTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "お値段設定"
        textField.textAlignment = .right
        textField.backgroundColor = .white
        textField.keyboardType = UIKeyboardType.numberPad
        textField.addTarget(self, action: #selector(handleProfitView), for: .editingChanged)
        return textField
    }()

    let profitTextView: UITextView = {
        let pv = UITextView()
        pv.font = UIFont.systemFont(ofSize: 16)
        pv.text = "利益"
        pv.font = UIFont.systemFont(ofSize: 16)
        pv.textColor = UIColor.lightGray
        //pv.textColor = UIColor.black
        pv.textAlignment = .right
        pv.keyboardType = UIKeyboardType.numberPad
        pv.isEditable = false
        return pv
    }()
    
    @objc func handleProfitView() {
       
        //let a:Int? = Int(profitTextView.text!)
        guard let priceText = priceTextField.text else {return}
        guard let priceField = Double(priceText) else {return}
        let profit: Double = priceField * Double(0.9)
        let intProfit = Int(profit)
        
//        guard let profitField = Double(profitTextView.text) else {return}
//        var profits: Double = profitField * profit
       
        //profitTextView.text = "利益: ¥\(profit)"
        profitTextView.text = String(intProfit)
        
        if priceTextField.text?.count == 1 {
            profitTextView.text = String(0)
        }
        
        print(profitTextView.text)
    
    }
    
    func updateFromTime() {
        let timeSelectorController = TimeSelectorController()
        timeSelectorController.timeDelegate = self
        print("coming from timeShareController")
    }
    
    
    fileprivate func setupImageAndTextViews() {
        
        let containerView = UIView()
        containerView.backgroundColor = .white
        
        let containerView2 = UIView()
        containerView2.backgroundColor = .red
        
        view.addSubview(containerView)
        containerView.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 100, width: 0)
        
        containerView.addSubview(imageView)
        imageView.anchor(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: nil, paddingTop: 8, paddingLeft: 8, paddingBottom: 8, paddingRight: 0, height: 0, width: 84)
        
        containerView.addSubview(textView)
        textView.anchor(top: containerView.topAnchor, left: imageView.rightAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, paddingTop: 0, paddingLeft: 4, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        view.addSubview(containerView2)
        containerView2.anchor(top: containerView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 500, width: 0)
        
        containerView2.addSubview(titleView)
        titleView.anchor(top: containerView2.topAnchor, left: containerView2.leftAnchor, bottom: nil, right: containerView2.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: containerView2.frame.width)
        
        containerView2.addSubview(categoryView)
        categoryView.anchor(top: titleView.bottomAnchor, left: containerView2.leftAnchor, bottom: nil, right: containerView2.rightAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: containerView2.frame.width)
        
        containerView2.addSubview(placeView)
        placeView.anchor(top: categoryView.bottomAnchor, left: containerView2.leftAnchor, bottom: nil, right: containerView2.rightAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: containerView2.frame.width)
        
        containerView2.addSubview(languageView)
        languageView.anchor(top: placeView.bottomAnchor, left: containerView2.leftAnchor, bottom: nil, right: containerView2.rightAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: containerView2.frame.width)
        
        containerView2.addSubview(participantsView)
        participantsView.anchor(top: languageView.bottomAnchor, left: containerView2.leftAnchor, bottom: nil, right: containerView2.rightAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: containerView2.frame.width)
        
        containerView2.addSubview(scheduleButton)
        scheduleButton.anchor(top: participantsView.bottomAnchor, left: containerView2.leftAnchor, bottom: nil, right: containerView2.rightAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: containerView2.frame.width)
        
        containerView2.addSubview(priceTextField)
        priceTextField.anchor(top: scheduleButton.bottomAnchor, left: containerView2.leftAnchor, bottom: nil, right: containerView2.rightAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: containerView2.frame.width)
        
        containerView2.addSubview(profitTextView)
        profitTextView.anchor(top: priceTextField.bottomAnchor, left: containerView2.leftAnchor, bottom: nil, right: containerView2.rightAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: containerView2.frame.width)
    }
    
    
    @objc func handleShare() {
        
        guard let caption = textView.text, caption.count > 0 else {return}
        guard let image = selectedImage else {return}
        guard let title = titleView.text, title.count > 0 else {return}
        guard let category = categoryView.text, category.count > 0 else {return}
        guard let place = placeView.text, place.count > 0 else {return}
        guard let language = languageView.text, language.count > 0 else {return}
        guard let participant = participantsView.text, participant.count > 0 else {return}
        //guard let partNumber = participantsView.text?.removeLast() else {return}
        //guard let participants = partNumber else {return}
        guard let priceNumber = UInt(priceTextField.text!) , priceNumber > 0 else {return}
        guard let profit = UInt(profitTextView.text), profit > 0 else {return}
        //guard let fromTime = timeSelectorCell.fromTextView.text, fromTime.count > 0 else {return}
        guard let uploadData = image.jpegData(compressionQuality: 0.5) else {return}
        
        navigationItem.rightBarButtonItem?.isEnabled = false
        
        let filename = NSUUID().uuidString
        //Storage.storage().reference().child("posts").child(filename).putData(uploadData, metadata: nil) { (metadata, err) in
        
        let storageRef = Storage.storage().reference().child("profile_images").child(filename)
        storageRef.putData(uploadData, metadata: nil) { (metadata, err) in
            
            if let err = err {
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                print("Failed to upload image", err)
                return
            }
            
            storageRef.downloadURL(completion: { (downloadURL, err) in
                guard let imageUrl = downloadURL?.absoluteString else {return}
                
                print("Successfully uploaded post image", imageUrl)
    
                self.saveToDatabaseWithImageUrl(imageUrl: imageUrl)
//            })
//
//            Storage.storage().reference().child("posts").child(filename).downloadURL(completion: { (url, err) in
//                if let err = err {
//                    print("failed to get downloadurl", err)
//                } else {
//                    guard let imageUrl = url?.absoluteString else {return}
                
                
                
            })
            //guard let imageUrl = metadata?.downloadURL()?.absoluteString else {return}
            
        }
        
    }
    
    let timeSelectorCell = TimeSelectorCell()
    
    var selectTIme: Date?
    
    static let updateFeedNotificationName = NSNotification.Name(rawValue: "UpdateFeed")
    
    fileprivate func saveToDatabaseWithImageUrl(imageUrl: String) {
        
        guard let postImage = selectedImage else {return}
        guard let caption = textView.text else {return}
        guard let title = titleView.text else {return}
        guard let category = categoryView.text else {return}
        guard let place = placeView.text else {return}
        guard let language = languageView.text else {return}
        guard let accountId = accountIdView.text else {return}
        
        //let partNumber = participantsView.text?.removeFirst()
        //guard let participant = partNumber else {return}
        let participant = participantsView.text.filter{ $0 != "人"}
        guard let participantInt = UInt(participant) else {return}
        //guard let participant = participantsView.text else {return}
        
        guard let priceNumber = UInt(priceTextField.text!) else {return}
        let price = priceNumber
        
        guard let profit = UInt(profitTextView.text) else {return}
        
        print(title)
        print(category)
        print(place)
        print(participantInt)
        print(price)
        print(profit)
        print(accountId)
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        let userPostRef = Database.database().reference().child("posts").child(uid)
        let ref = userPostRef.childByAutoId()
        let values = ["accountId": accountId, "imageUrl": imageUrl, "caption": caption, "title": title, "category": category, "place": place, "language": language, "participant": participantInt, "price": price, "profit": profit, "imageWidth": postImage.size.width, "imageHeight": postImage.size.height, "creationDate": Date().timeIntervalSince1970] as [String : Any]
        ref.updateChildValues(values) { (err, ref) in
            if let err = err {
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                print("failed to save post to DB", err)
                return
            }
            print("Successfully save post to DB")
            self.dismiss(animated: true, completion: nil)
            //(Episode 28) how to load post automatically: part1
            //let name = NSNotification.Name(rawValue: "UpdateFeed")
            NotificationCenter.default.post(name: SharePhotoController.updateFeedNotificationName, object: nil)
        }
    }
}
