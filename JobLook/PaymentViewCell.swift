//
//  PaymentViewCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/10/18.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

protocol PaymentViewCellDelegate {
    func didTapAddPayment()
    func didTapPurchase(for amountCharge: String, for cardId: String)
    //func didTapMessage()
    func didTapBack()
}

class PaymentViewCell: UICollectionViewCell {
    
    var post: Post? {
        didSet {
            checkTitleLabel.text = post?.title
            
            checkPlaceLabel.text = post?.place
            checkDateLabel.text = "2018/11/11"
            
            guard let imageUrl = post?.imageUrl else {return}
            imageView.loadImage(urlString: imageUrl)
            
            //guard let price = post?.price else {return}
            //priceLabel.text = String(price)
            
            guard let convert = post?.convert else {return}
            print("this is convert value", convert)
            
            guard let price = post?.price else {return}
            let convertedPrice = convert * Double(price)
            priceLabel.text = "\((convertedPrice * 100).rounded() / 100)"
            
            //priceLabel.text = "¥\(String(price))"
            
            switch post?.currencyMark {
            case "GBP":
                markLabel.text = "€"
            case "USD":
                markLabel.text = "$"
            case "JPY":
                markLabel.text = "¥"
            default:
                markLabel.text = "¥"
            }
            
            //guard let cardId = card?.cardId else {return}
            //addCardButton.setTitle(cardId, for: .normal)
        }
    }
    var delegate: PaymentViewCellDelegate?
    
    let overViewLabel: UILabel = {
        let label = UILabel()
        label.text = "Overview"
        label.font = UIFont.boldSystemFont(ofSize: 30)
        //label.backgroundColor = UIColor.gray
        return label
    }()
    
    let checkTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Tokyo Walk"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        //label.backgroundColor = UIColor.gray
        label.numberOfLines = 0
        return label
    }()
    
    let checkPlaceLabel: UILabel = {
        let label = UILabel()
        label.text = "@ Shibuya"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        //label.backgroundColor = UIColor.gray
        label.numberOfLines = 0
        return label
    }()
    
    let checkDateLabel: UILabel = {
        let label = UILabel()
        label.text = "2018/10/30"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        //label.backgroundColor = UIColor.gray
        label.numberOfLines = 0
        return label
    }()
    
    let imageView: CustomImageView = {
        let iv = CustomImageView()
        iv.backgroundColor = .cyan
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
        
    }()
    
    lazy var addCardButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Add Payment/Chose Payment", for: .normal)
        //button.titleLabel?.text = "Add Payment"
        button.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handleAddCard), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handleAddCard() {
        print("handling add card payment")
        
        delegate?.didTapAddPayment()
        
    }
    
    let markLabel: UILabel = {
        let label = UILabel()
        label.text = "¥"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textAlignment = .right
        label.backgroundColor = UIColor.lightGray
        label.textColor = .black
        return label
    }()
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.text = "5000"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textAlignment = .right
        label.backgroundColor = UIColor.lightGray
        return label
    }()
    
    lazy var purchaseButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Make Payment", for: .normal)
        button.backgroundColor = UIColor.cyan
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handlePurchase), for: .touchUpInside)
        return button
    }()
    
//    lazy var startMessageButton: UIButton = {
//        let button = UIButton(type: .system)
//        button.setTitle("Start Message", for: .normal)
//        button.backgroundColor = .brown
//        button.setTitleColor(.white, for: .normal)
//        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
//        button.layer.cornerRadius = 5
//        button.addTarget(self, action: #selector(handleMessage), for: .touchUpInside)
//        return button
//    }()
    
    lazy var backToButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Back to Home", for: .normal)
        button.backgroundColor = UIColor.red
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        return button
    }()
    
    
    @objc fileprivate func handlePurchase() {
        print("handling purchase")
        guard let amount = priceLabel.text else {return}
        guard let id = addCardButton.titleLabel?.text else {return}
        delegate?.didTapPurchase(for: amount, for: id)
        
    }
    
//    @objc fileprivate func handleMessage() {
//        print(123)
//        delegate?.didTapMessage()
//    }
    
    @objc fileprivate func handleBack() {
        print("handling back")
        
        delegate?.didTapBack()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //backgroundColor = UIColor.orange
        
        setupView()
        
    }
    
    func setupView() {
        
        let containerView = UIView()
        containerView.backgroundColor = .white
        
        addSubview(overViewLabel)
        overViewLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, height: 40, width: 0)
        
        addSubview(containerView)
        containerView.anchor(top: overViewLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 100, width: 0)
        
        let seperatorView = UIView()
        seperatorView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        addSubview(seperatorView)
        seperatorView.anchor(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0.5, width: 0)
        
        addSubview(checkTitleLabel)
        checkTitleLabel.anchor(top: seperatorView.topAnchor, left: containerView.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(checkPlaceLabel)
        checkPlaceLabel.anchor(top: checkTitleLabel.bottomAnchor, left: containerView.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(checkDateLabel)
        checkDateLabel.anchor(top: checkPlaceLabel.bottomAnchor, left: containerView.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(imageView)
        imageView.anchor(top: containerView.topAnchor, left: checkTitleLabel.rightAnchor, bottom: nil, right: containerView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 100, width: 80)
        
        let seperatorView2 = UIView()
        seperatorView2.backgroundColor = UIColor(white: 0, alpha: 0.5)
        addSubview(seperatorView2)
        seperatorView2.anchor(top: containerView.bottomAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0.5, width: 0)
        
        addSubview(addCardButton)
        addCardButton.anchor(top: containerView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 50, width: 0)
        
        addSubview(markLabel)
        markLabel.anchor(top: addCardButton.bottomAnchor, left: nil, bottom: nil, right: nil, paddingTop: 80, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: frame.width)
        
        addSubview(priceLabel)
        priceLabel.anchor(top: addCardButton.bottomAnchor, left: markLabel.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 80, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
        
        addSubview(purchaseButton)
        purchaseButton.anchor(top: priceLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 50, width: 0)
        
//        addSubview(startMessageButton)
//        startMessageButton.anchor(top: purchaseButton.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 50, width: 0)
        
        addSubview(backToButton)
        backToButton.anchor(top: purchaseButton.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 50, width: 0)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
