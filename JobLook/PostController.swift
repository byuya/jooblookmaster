//
//  PostController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/30.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class PostController: UICollectionViewController, UICollectionViewDelegateFlowLayout, PostHeaderDelegate, PostFooterCellDelegate, PostCellDelegate {
    
    func didTapGuestButton(post: Post) {
        print("coming from PostCell")
    }
    
//    func didTapGuestButton() {
//        print("Coming from postcell")
//    }
    
    
    let cellId = "cellId"
    let headerId = "headerId"
    let footerId = "footerId"
    
    var message: Message?
    var post: Post?
    //var postId: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .white
        collectionView.alwaysBounceVertical = true
        
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 80, right: 0)
        
        collectionView.register(PostCell.self, forCellWithReuseIdentifier: cellId)
        
        //collectionView.register(PostHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerId)
        
//        collectionView.register(FooterCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerId)
        //collectionView.register(FooterCell.self, forCellWithReuseIdentifier: footerId)
    
        setupInputView()
               
    }
    
    var posts = [Post]()

    func setupInputView() {
        
        let containerView: UIView = {
            let containerView = UIView()
            containerView.backgroundColor = UIColor.mainBlue()
            containerView.frame = CGRect(x: 0, y: 0, width: 100, height: 50)
            
            let button = UIButton(type: .system)
            button.setImage(#imageLiteral(resourceName: "right_arrow_shadow").withRenderingMode(.alwaysOriginal), for: .normal)
            //button.setTitle("Purchase", for: .normal)
            button.tintColor = UIColor(white: 0, alpha: 0.2)
            button.addTarget(self, action: #selector(handlePurchase), for: .touchUpInside)
            containerView.addSubview(button)
            button.anchor(top: containerView.topAnchor, left: nil, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 10, height: 0, width: 0)
            
            let contactButton = UIButton(type: .system)
            contactButton.setTitle("Message to Host", for: .normal)
            //contactButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            //contactButton.titleLabel?.backgroundColor = .white
            contactButton.backgroundColor = .white
            contactButton.tintColor = UIColor(white: 0, alpha: 0.2)
            contactButton.layer.masksToBounds = true
            contactButton.layer.cornerRadius = 5
            contactButton.addTarget(self, action: #selector(handleMessage), for: .touchUpInside)
            containerView.addSubview(contactButton)
            contactButton.anchor(top: containerView.topAnchor, left: nil, bottom: containerView.bottomAnchor, right: button.leftAnchor, paddingTop: 5, paddingLeft: 0, paddingBottom: 5, paddingRight: 10, height: 30, width: 150)
            
            
            
            if let price = post?.price, let currencyMark = post?.currencyMark, let convert = post?.convert {
                
                let markLabel = UILabel()
                //markLabel.text = "¥"
                markLabel.font = UIFont.systemFont(ofSize: 20)
                markLabel.textColor = .white
                markLabel.textAlignment = .left
                containerView.addSubview(markLabel)
                markLabel.anchor(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
                
                switch currencyMark {
                case "USD":
                    markLabel.text = "$"
                case "GBP":
                    markLabel.text = "€"
                case "JPY":
                    markLabel.text = "¥"
                default:
                    markLabel.text = "¥"
                }
             
                
                let convertedPrice = Double(price) * convert
                
                print("this is converted price", price, convertedPrice)
                //let priceInt = String(price)

                let priceLabel = UILabel()
                priceLabel.text = "\((convertedPrice * 100).rounded() / 100)"
                priceLabel.font = UIFont.systemFont(ofSize: 20)
                priceLabel.textColor = .white
                priceLabel.textAlignment = .left
                containerView.addSubview(priceLabel)
                priceLabel.anchor(top: containerView.topAnchor, left: markLabel.rightAnchor, bottom: containerView.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 2, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
                
               
            }
           
            
            return containerView
        }()
    
        view.addSubview(containerView)
        containerView.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: view.frame.width)
        
    }
    
    @objc fileprivate func handlePurchase() {
        print("Tapping handle purchase buttoon")
        
        let paymentController = PaymentViewController(collectionViewLayout: UICollectionViewFlowLayout())
        paymentController.post = post
        navigationController?.pushViewController(paymentController, animated: true)
        
//        let chooseDateController = ChooseDateController(collectionViewLayout: UICollectionViewFlowLayout())
//        navigationController?.pushViewController(chooseDateController, animated: true)
       
    }
    
    @objc func handleMessage() {
        
        let commentsController = CommentsController(collectionViewLayout: UICollectionViewFlowLayout())
        commentsController.post = post
        navigationController?.pushViewController(commentsController, animated: true)
        
    }
    
    func didTapPurchase(for footer: FooterCell) {
        print("Tapping purchase")
        
    }
    
    let footer: PostFooter = {
        let ft = PostFooter()
        return ft
    }()

    func setupFooter() {
        view.addSubview(footer)
        footer.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 70, width: view.frame.width)
    }
    
//    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//
//        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath)
//        as! PostHeader
//
//        header.delegate = self
//
//        return header
//    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: view.frame.width, height: 50)
//
//    }
    
    func didTapBack() {
        //self.dismiss(animated: true, completion: nil)
        //let userProfileController = UserProfileController(collectionViewLayout: UICollectionViewFlowLayout())
//        navigationController?.present(userProfileController, animated: true, completion: nil)
        //userProfileController.dismiss(animated: true, completion: nil)
       //let postController = PostController(collectionViewLayout: UICollectionViewFlowLayout())
        //self.dismiss(animated: true, completion: nil)
        print("Tapping on dismiss button from Post Header")
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
        //return posts.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PostCell

        cell.post = self.post
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 1200)
    }
    
   
     let poseCell = PostCell()
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        
    }
    
    
//    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PostCell
//
//        //cell.post = posts[indexPath.item]
//
//        if indexPath.row == 0 {
//
//            if let imageUrl = post?.imageUrl {
//                cell.imageView.loadImage(urlString: imageUrl)
//            }
//            cell.titleLabel.text = post?.title
//            cell.categoryLabel.text = post?.category
//            cell.placeLabel.text = post?.place
//            cell.languageLabel.text = post?.language
//
//            cell.hostLabel.text = nil
//            cell.hostTitleLabel.text = nil
//            cell.hostImageView.isHidden = true
//            cell.postTitleLabel.text = nil
//            cell.postDescriptionLabel.text = nil
//            cell.hostProvideTitleLabel.text = nil
//            cell.hostProvideLabel.text = nil
//            cell.participantLabel.text = nil
//            cell.guestRequirementTitleLabel.text = nil
//
//             return cell
//
//
//        } else if indexPath.row == 1 {
//
//            if let profileImageUrl = post?.user.profileImageUrl {
//                cell.hostImageView.loadImage(urlString: profileImageUrl)
//            }
//
//            cell.backgroundColor = .brown
//            cell.imageView.isHidden = true
//            cell.titleLabel.text = nil
//            cell.categoryLabel.text = nil
//            cell.placeLabel.text = nil
//            cell.languageLabel.text = nil
//            cell.hostLabel.text = "Hi, I`m Akiya 26 years old, a Japanese host for 2 years. I will be guiding a city of Shibuya, one of the most crowded and entertained place in Japan. I can guide you in both English and Japanese."
//            //cell.hostDescriptionLabel.text = nil
//            cell.postTitleLabel.text = nil
//            cell.postDescriptionLabel.text = nil
//            cell.hostProvideTitleLabel.text = nil
//            cell.hostProvideLabel.text = nil
//            cell.participantLabel.text = nil
//            cell.guestRequirementTitleLabel.text = nil
//
//            return cell
//
//
//        } else if indexPath.row == 2 {
//
//            cell.postDescriptionLabel.text = post?.caption
//
//            cell.backgroundColor = .purple
//            cell.imageView.isHidden = true
//            cell.hostImageView.isHidden = true
//            cell.titleLabel.text = nil
//            cell.categoryLabel.text = nil
//            cell.placeLabel.text = nil
//            cell.languageLabel.text = nil
//            cell.hostLabel.text = nil
//            cell.hostTitleLabel.text = nil
//            //cell.postDescriptionLabel.text = "This experience contains two part. 1: I will be guiding you the most popular Sake tasting bar in Shibuya. you will know the best way to taste Sake as well as history of it."
//            cell.hostProvideTitleLabel.text = nil
//            cell.hostProvideLabel.text = nil
//            cell.participantLabel.text = nil
//            cell.guestRequirementTitleLabel.text = nil
//
//            return cell
//
//        } else if indexPath.row == 3 {
//            cell.backgroundColor = UIColor.lightGray
//            cell.imageView.isHidden = true
//            cell.hostImageView.isHidden = true
//            cell.titleLabel.text = nil
//            cell.categoryLabel.text = nil
//            cell.placeLabel.text = nil
//            cell.languageLabel.text = nil
//            cell.hostLabel.text = nil
//            cell.hostTitleLabel.text = nil
//            cell.postTitleLabel.text = nil
//            cell.postDescriptionLabel.text = nil
//            cell.participantLabel.text = nil
//            cell.guestRequirementTitleLabel.text = nil
//
//            return cell
//
//        } else if indexPath.row == 4 {
//            cell.backgroundColor = UIColor.darkGray
//            cell.imageView.isHidden = true
//            cell.hostImageView.isHidden = true
//            cell.titleLabel.text = nil
//            cell.categoryLabel.text = nil
//            cell.placeLabel.text = nil
//            cell.languageLabel.text = nil
//            cell.hostLabel.text = nil
//            cell.hostTitleLabel.text = nil
//            cell.postTitleLabel.text = nil
//            cell.postDescriptionLabel.text = nil
//            cell.hostProvideTitleLabel.text = nil
//            cell.hostProvideLabel.text = nil
//            cell.guestRequirementTitleLabel.text = nil
//
//            return cell
//
//        } else if indexPath.row == 5 {
//            cell.backgroundColor = UIColor.magenta
//            cell.imageView.isHidden = true
//            cell.hostImageView.isHidden = true
//            cell.titleLabel.text = nil
//            cell.categoryLabel.text = nil
//            cell.placeLabel.text = nil
//            cell.languageLabel.text = nil
//            cell.hostLabel.text = nil
//            cell.hostTitleLabel.text = nil
//            cell.postTitleLabel.text = nil
//            cell.postDescriptionLabel.text = nil
//            cell.hostProvideTitleLabel.text = nil
//            cell.hostProvideLabel.text = nil
//            cell.participantLabel.text = nil
//
//            return cell
//
//        }
//        cell.imageView.isHidden = false
//
//        return cell
//
//    }
    

//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        if indexPath.row == 0 {
//            return CGSize(width: view.frame.width, height: 400)
//        } else if indexPath.row == 1 {
//            return CGSize(width: view.frame.width, height: 200)
//        } else if indexPath.row == 2 {
//            return CGSize(width: view.frame.width, height: 200)
//        } else if indexPath.row == 3 {
//            return CGSize(width: view.frame.width, height: 200)
//        } else if indexPath.row == 4 {
//            return CGSize(width: view.frame.width, height: 80)
//        } else if indexPath.row == 5 {
//            return CGSize(width: view.frame.width, height: 80)
//        }
//            return CGSize(width: view.frame.width, height: 400)
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //navigationController?.navigationBar.isHidden = true
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
        
    }
    
    
}
