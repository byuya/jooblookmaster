//
//  FeedbackController.swift
//  JobLook
//
//  Created by Akiya Ozawa on 10/23/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit
import Stripe
import Firebase
import FirebaseAuth
import FirebaseDatabase

class FeedbackController: UICollectionViewController, UICollectionViewDelegateFlowLayout, FeedbackViewCellDelegate {
    
    var post: Post?
    var experiment: Experiment?
    
    let cellId = "cellId"
    
    let accountText: UILabel = {
        let text = UILabel()
        return text
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .white
        
        collectionView.register(FeedbackViewCell.self, forCellWithReuseIdentifier: cellId)
        
        fetchExperienceData()
            
    }
    
    fileprivate func fetchExperienceData() {
        print(123)
        
        guard let postId = post?.user.uid else {return}
        guard let uid = Auth.auth().currentUser?.uid else {return}
        print(postId)
        
        let feedRef = Database.database().reference().child("experiment").child(uid).child(postId)
        feedRef.observe(.value, with: { (snapshot) in
            
            guard let dictionaries = snapshot.value as? [String: Any] else {return}
            dictionaries.forEach({ (key, value) in
                guard let dictionary = value as? [String : Any] else {return}
                Database.fetchUserWithUID(uid: uid, completion: { (user) in
                     var experiment = Experiment(user: user, dictionary: dictionary)
                        experiment.id = key
                    
                    self.experiments.append(experiment)
                    self.collectionView.reloadData()
                })
               
            })
        }) { (err) in
            print("failed to fetch experiment data")
        }
    }
    
    func didTapCancel(for cell: FeedbackViewCell) {
        print("coming from feedbackcell")
        
         guard let indexPath =  collectionView.indexPath(for: cell) else {return}
        
        let experiment = self.experiments[indexPath.item]
        print(experiment.accountId)
        
        let cancelViewController = CancelViewController(collectionViewLayout: UICollectionViewFlowLayout())
        cancelViewController.experiment = experiment
        //cancelViewController. = post?.user.uid
        cancelViewController.postId = post?.user.uid
        navigationController?.present(cancelViewController, animated: true, completion: nil)
//
//        guard let uid = Auth.auth().currentUser?.uid else {return}
//        guard let destination = self.accountText.text else {return}
//
//        guard let intAmount = (post?.price) else {return}
//        let amount = (UInt(Double(intAmount) * 0.9))
//
//        print(destination, amount)
//
//        let values = ["amount": amount, "destination": destination] as [String: Any]
//        let ref = Database.database().reference().child("users").child(uid).child("transfers")
//        ref.childByAutoId().updateChildValues(values) { (err, ref) in
//            if let err = err {
//                print("failed to insert transfer data", err)
//            }
//
//                //successfully tranfered
//                print("successfully inserted transfer data")
//
//        }
    }
    
    var experiments = [Experiment]()
    var posts = [Post]()
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! FeedbackViewCell
        cell.experiment = experiments[indexPath.item]
        //cell.post = self.post
        cell.delegate = self
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return experiments.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(123)
        
        let experiment = experiments[indexPath.item]
        let post = self.post
        let sendFeedbackController = SendFeedbackController(collectionViewLayout: UICollectionViewFlowLayout())
        sendFeedbackController.experiment = experiment
        sendFeedbackController.experimentId = experiment.id
        sendFeedbackController.postId = post?.user.uid
        
        navigationController?.pushViewController(sendFeedbackController, animated: true)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 150)
    }
    
}
