//
//  HostRegisterController.swift
//  JobLook
//
//  Created by Akiya Ozawa on 10/22/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import Stripe

class HostRegisterController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate, UIImagePickerControllerDelegate, HostRegisterCellDelegate, UIPickerViewDataSource, UIPickerViewDelegate  {
    
    let cellId = "cellId"
    
    let gender = ["", "male", "female"]
    
    var selectedGender: String?
    var selectedBirthDay: String?
    
    let pickerView = UIPickerView()
    
    let datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.translatesAutoresizingMaskIntoConstraints = false
        picker.addTarget(self, action: #selector(handleTime), for: .valueChanged)
        return picker
    }()
    
    @objc func handleTime() {
        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "yyyy/MM/dd"
        dateFormatter.dateFormat = "yyyy"
        selectedBirthDay = dateFormatter.string(from: datePicker.date)
        
        //self.collectionView.reloadData()
    }
    
    let toolBar: UIToolbar = {
        let tool = UIToolbar()
        tool.sizeToFit()
        return tool
    }()
    
    //登録ボタン
    let plusPhotoButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "plus_photo").withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(handlePlusPhoto), for: .touchUpInside)
        return button
    }()
    
    @objc func handlePlusPhoto() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        present(imagePickerController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        collectionView.alwaysBounceVertical = true
        
        collectionView.backgroundColor = .white
        collectionView.register(HostRegisterCell.self, forCellWithReuseIdentifier: cellId)
        
        setupPhotoView()
        
        pickerView.delegate = self
        
        let toolBarBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(handleTool))
        toolBar.setItems([toolBarBtn], animated: false)
        toolBar.isUserInteractionEnabled = true
    }
    
    @objc func handleTool() {
        view.endEditing(true)
        
        self.collectionView.reloadData()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return gender.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return gender[row]
    }

  
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        selectedGender = gender[row]
        print(selectedGender)
        
        //self.collectionView.reloadData()
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! HostRegisterCell
        cell.delegate = self
        
        cell.genderTextField.inputView = pickerView
        cell.genderTextField.text = selectedGender
        cell.genderTextField.inputAccessoryView = toolBar
        
        cell.dateBirthYear.inputView = datePicker
        cell.dateBirthYear.text = selectedBirthDay
        cell.dateBirthYear.inputAccessoryView = toolBar
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 1000)
    }
    
//    func didTapAddPicture() {
//        print("trying to pick picutre for identification")
//        let imagePickerController =  UIImagePickerController()
//        imagePickerController.delegate = self
//        imagePickerController.allowsEditing = true
//        present(imagePickerController, animated: true, completion: nil)
//    }
//
//
//
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {

            plusPhotoButton.setImage(editedImage.withRenderingMode(.alwaysOriginal), for: .normal)
            print("plusphotto button working", editedImage.size)

        } else if let originalImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {

            plusPhotoButton.setImage(originalImage.withRenderingMode(.alwaysOriginal), for: .normal)

        }
       
        //plusPhotoButton.layer.cornerRadius = plusPhotoButton.frame.width/2
        plusPhotoButton.layer.cornerRadius = 5
        plusPhotoButton.layer.masksToBounds = true
        plusPhotoButton.layer.borderColor = UIColor.black.cgColor
        plusPhotoButton.layer.borderWidth = 1
        dismiss(animated: true, completion: nil)

    }
  
    
    func didTapSubmit(for firstNameText: String,
                      for lastNameText: String,
                      for genderText: String,
                      for phoneText: String,
                      for dateBirthYearText: String,
                      for dateBirthMonthText: String,
                      for dateBirthDayText: String,
                      for addressKanjiStateText: String,
                      for addressKanjiCityText: String,
                      for addressKanjiTownText: String,
                      for addressKanaStateText: String,
                      for addressKanaCityText: String,
                      for addressKanaTownText: String,
                      for numberText: String,
                      for postalText: String) {

        print("coming from host register cell", firstNameText, lastNameText, genderText, phoneText, dateBirthYearText, dateBirthMonthText, dateBirthDayText, "state\(addressKanjiStateText)", addressKanjiCityText, addressKanjiTownText, addressKanaStateText, addressKanaCityText, addressKanaTownText, numberText, postalText)

//        let connectParams = STPConnectAccountParams(legalEntity: STPLegalEntityParams.init())
//
//        connectParams.legalEntity.firstName = firstNameText
//        connectParams.legalEntity.lastName = lastNameText
//        connectParams.legalEntity.genderString = genderText
//        connectParams.legalEntity.phoneNumber = phoneText
//        connectParams.legalEntity.address?.state = addressKanaStateText
//        connectParams.legalEntity.address?.city = addressKanaCityText
//        connectParams.legalEntity.address?.line1 = addressKanaTownText
//        connectParams.legalEntity.address?.line2 = numberText
//        connectParams.legalEntity.address?.postalCode = postalText
//        connectParams.legalEntity.dateOfBirth?.year = Int(dateBirthYearText)
//        connectParams.legalEntity.dateOfBirth?.month = Int(dateBirthMonthText)
//        connectParams.legalEntity.dateOfBirth?.day = Int(dateBirthDayText)
//
//        //connectParams.tosShownAndAccepted == true
//
//        STPAPIClient.shared().createToken(withConnectAccount: connectParams) { (token: STPToken?, err: Error?) in
//            guard let token = token, err == nil else {
//                print("failed to create token", err.debugDescription)
//                return
//            }
//
//        print("successfully created token", token.tokenId)
        
        //start storing idimage
        guard let idImage = self.plusPhotoButton.imageView?.image else {return}
        guard let uploadData = idImage.jpegData(compressionQuality: 0.3) else {return}
        
        print(idImage.size)
        
        let fileName = NSUUID().uuidString
        let storageRef = Storage.storage().reference().child("id_document").child(fileName)
        storageRef.putData(uploadData, metadata: nil) { (metadata, err) in
            if let err = err {
                print("failed to upload id_document", err)
                return
            }
            
            storageRef.downloadURL(completion: { (downloadURL, err) in
                guard let profileImageURL = downloadURL?.absoluteString else {return}
                print(profileImageURL)
                
                STPAPIClient.shared().uploadImage(idImage, purpose: STPFilePurpose.identityDocument) { (file: STPFile?, err: Error?) in
                    guard let file = file, err == nil else {
                        print("failed to create fileId")
                        return
                        
                    }
                    
                    print("Successfully get Stripe API Response", file.fileId)
                    
                    let doc = file.fileId
                    let type = "custom"
                    let firstName = firstNameText
                    let lastName = lastNameText
                    let phone = phoneText
                    let gender = genderText
                    let dateBirthYear = dateBirthYearText
                    let dateBirthMonth = dateBirthMonthText
                    let dateBirthDay = dateBirthDayText
                    let stateKanji = addressKanjiStateText
                    let cityKanji = addressKanjiCityText
                    let townKanji = addressKanjiTownText
                    let stateKana = addressKanaStateText
                    let cityKana = addressKanaCityText
                    let townKana = addressKanaTownText
                    let number = numberText
                    let postal = postalText
                    
                    let value = ["lastName": lastName, "firstName": firstName, "type": type, "gender": gender, "phone": phone, "dateBirthYear": dateBirthYear, "dateBirthMonth": dateBirthMonth, "dateBirthDay": dateBirthDay, "stateKanji": stateKanji, "cityKanji": cityKanji, "townKanji": townKanji, "stateKana": stateKana, "cityKana": cityKana, "townKana": townKana, "number": number, "postal": postal, "doc": doc] as [String: Any]
                    
                    guard let uid = Auth.auth().currentUser?.uid else {return}
                    
                    let ref = Database.database().reference().child("users").child(uid).child("host")
                    ref.updateChildValues(value) { (err, ref) in
                        if let err = err {
                            print("failed to store host info to db", err)
                        }
                        
                        print("successfully stored host info into db", ref)
                    }
                }
                
            })

        //}
            }
    }
    
    //                guard let url = URL(string: "https://stripe.com/img/docs/connect/success.png") else {return}
    //
    //                if let data = try? Data(contentsOf: url)
    //                {
    //                    let img: UIImage = UIImage(data: data)!
    //                    //let urlString = url.absoluteString
    //                    print(img.imageAsset)
    //    }
    
    func didTapAcceptanceTerm() {
        print("show acceptance term")
        let acceptanceTerm = AcceptanceTerm(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.pushViewController(acceptanceTerm, animated: true)
    }
    
    
    func didTapBankSubmit(for accountNumberText: String, for routingNumberText: String, for accountNameText: String) {
        print("coming from hostBankAccountCell", accountNumberText, routingNumberText, accountNameText)
        
        let bankParams = STPBankAccountParams()
        
        bankParams.accountNumber = accountNumberText
        bankParams.routingNumber = routingNumberText
        bankParams.accountHolderName = accountNameText
        
        bankParams.country = "JP"
        bankParams.currency = "jpy"
        
        STPAPIClient.shared().createToken(withBankAccount: bankParams) { (bankToken: STPToken?, error: Error?) in
            guard let bankToken = bankToken, error == nil else {
                print("failed to get banktoken", error ?? "")
                return
            }
            
            print("successfully get banktoken", bankToken.tokenId)
            
            let value = ["token": bankToken.tokenId]
            
            guard let uid = Auth.auth().currentUser?.uid else {return}
            let bankRef = Database.database().reference().child("users").child(uid).child("bank")
            bankRef.childByAutoId().updateChildValues(value, withCompletionBlock: { (err, ref) in
                if let err = err {
                    print("failed to insert bank info into db", err)
                }
                
                print("successfully insered bank info into db")
            })
        }
        
    }
    
    func setupPhotoView() {
        
        collectionView.addSubview(plusPhotoButton)
        plusPhotoButton.anchor(top: collectionView.topAnchor, left: nil, bottom: nil, right: nil, paddingTop: 510, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 150, width: 250)
        plusPhotoButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
        
       
    
}
