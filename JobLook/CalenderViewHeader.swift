//
//  CalenderViewHeader.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/15.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

protocol CalenderViewHeaderDelegate {
    
    func didTapPrevMonthButton()
    func didTapNextMonthButton()
    
}
class CalenderViewHeader: UICollectionViewCell {
    
    var delegate: CalenderViewHeaderDelegate?
    
    lazy var prevMonthButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("前月", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(handlePrevMonthButton), for: .touchUpInside)
        return button
    }()
    
    lazy var nextMonthButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("次月", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(handleNextMonthButton), for: .touchUpInside)
        return button
    }()
    
    let headerTitleMonthLabel: UILabel = {
        let label = UILabel()
        //label.font = UIFont(name: "8/2018", size: 14)
        label.text = ""
        label.textColor = .black
        return label
    }()
    
    @objc func handlePrevMonthButton() {
        delegate?.didTapPrevMonthButton()

    }
    
    @objc func handleNextMonthButton() {
        delegate?.didTapNextMonthButton()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .cyan
        
        addSubview(prevMonthButton)
        prevMonthButton.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 16, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(nextMonthButton)
        nextMonthButton.anchor(top: topAnchor, left: nil, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 16, height: 0, width: 0)
        
        addSubview(headerTitleMonthLabel)
        headerTitleMonthLabel.anchor(top: topAnchor, left: nil, bottom: bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        headerTitleMonthLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
