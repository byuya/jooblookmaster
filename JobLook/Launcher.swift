//
//  Launcher.swift
//  JobLook
//
//  Created by Akiya Ozawa on 12/2/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase

class Launcher: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, LauncherCellDelegate {
    
    let blackView = UIView()
    
    let collectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        cv.backgroundColor = .white
        return cv
    }()
    
    let cellId = "cellId"
    
    @objc func handleWindow() {
        
        if let window = UIApplication.shared.keyWindow {
            
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
            
            window.addSubview(blackView)
            window.addSubview(collectionView)
            let height: CGFloat = 250
            let y = window.frame.height - height
            collectionView.frame = CGRect(x:0, y: window.frame.height, width: window.frame.width, height: height)
            
//            collectionView.anchor(top: nil, left: window.leftAnchor, bottom: nil, right: window.rightAnchor, paddingTop: 0, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 180, width: 180)
//
//            collectionView.centerXAnchor.constraint(equalTo: window.centerXAnchor).isActive = true
//            collectionView.centerYAnchor.constraint(equalTo: window.centerYAnchor).isActive = true
//            collectionView.layer.masksToBounds = true
//            collectionView.layer.cornerRadius = 10
            
            blackView.frame = window.frame
            blackView.alpha = 0
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                
                self.collectionView.frame = CGRect(x: 0, y: y, width: self.collectionView.frame.width, height: self.collectionView.frame.height)
                self.collectionView.layer.cornerRadius = 10
                self.collectionView.layer.masksToBounds = true
            }, completion: nil)
           
        }
    }
    
    @objc func handleDismiss() {
        UIView.animate(withDuration: 0.5) {
            self.blackView.alpha = 0
            
            if let window = UIApplication.shared.keyWindow {
                
                self.collectionView.frame = CGRect(x: 0, y: window.frame.height, width: self.collectionView.frame.width, height: self.collectionView.frame.height)
                
//                self.collectionView.anchor(top: window.topAnchor, left: nil, bottom: nil, right: nil, paddingTop: -200, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
//                self.collectionView.centerXAnchor.constraint(equalTo: window.centerXAnchor).isActive = false
//                self.collectionView.centerYAnchor.constraint(equalTo: window.centerYAnchor).isActive = false
            
            }
            
        }
    }
    
    static let callCancelRequest = NSNotification.Name(rawValue: "callCancelRequest")
    
    func didTapAccept() {
        print("coming from launcherdelegate")
        handleDismiss()
                
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            //calling cancelRequest function in PurchaseDetailController
           NotificationCenter.default.post(name: Launcher.callCancelRequest, object: nil)
            
        })
        
        
    }
    
    func didTapDismiss() {
        print("coming form launcherdelegate")
        handleDismiss()
    }
    
    override init() {
        super.init()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.register(LauncherCell.self, forCellWithReuseIdentifier: cellId)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! LauncherCell
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 50, height: 250)
    }
   
}
