//
//  CommentsController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/07/29.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class CommentsController: UICollectionViewController, UICollectionViewDelegateFlowLayout, CommentInputAccessoryViewDelegate{
    
    var ref: DatabaseReference!
    var decrementRef: DatabaseReference!
    
    var handleRef: DatabaseHandle!
    var handleDecrementRef: DatabaseHandle!
    
    var post: Post?
    var commentHistory: CommentHistory?
    
    let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .white
        collectionView.alwaysBounceVertical = true
        collectionView.keyboardDismissMode = .interactive
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
        
        //collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: -50, right: 0)
    
//        self.navigationItem.hidesBackButton = true
//        let newBackButtoon = UIBarButtonItem(title: "<Back", style: .plain, target: self, action: #selector(handleBack))
//        self.navigationItem.leftBarButtonItem = newBackButtoon
        
        collectionView.register(CommentCell.self, forCellWithReuseIdentifier: cellId)
        
        //fetchComments()
        
        fetchComment()
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let toId = commentHistory?.toId else {return}
        guard let postUser = commentHistory?.postUser else {return}
        guard let postId = commentHistory?.id else {return}
        guard let readCount = commentHistory?.read else {return}
        
        
        ref = Database.database().reference().child("user-comment").child(uid)
        decrementRef = Database.database().reference().child("user-comment").child(uid).child(toId).child(postUser).child(postId)
        
        //decrementTotalCount(readCount: readCount)
        //decrementHomeBadge(readCount: readCount)
        //decrementIfChange()
        
        print("this is what title", commentHistory?.testPost.title, "toid", commentHistory?.toId, "postId", commentHistory?.id, "postUser", commentHistory?.postUser, "reacCount", commentHistory?.read)
    }
    
    @objc func handleRefresh() {
        self.comments.removeAll()
        
        fetchComment()
        decrementAfterRefreshing()
    }
    
    @objc func handleBack() {
        
        //let checkCommentController = CheckCommentController(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.popToRootViewController(animated: true)
        //navigationController?.popToViewController(checkCommentController, animated: true)
        //navigationController?.popViewController(animated: true)
        
    }
    
    func decrementAfterRefreshing() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let toId = commentHistory?.toId else {return}
        guard let postUser = commentHistory?.postUser else {return}
        guard let postId = commentHistory?.id else {return}
        
        let ref = Database.database().reference().child("user-comment").child(uid).child(toId).child(postUser).child(postId)
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard let dictionary = snapshot.value as? [String: Any] else {return}
            
            if let read = dictionary["read"] as? Int {
                self.decrementTotalCount(readCount: read)
                self.decrementHomeBadge(readCount: read)
            }
        
        }) { (err) in
            print("failed to fetch data", err.localizedDescription)
            return
        }
       
    }
    
    func decrementTotalCount(readCount: Int) {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        //guard let readCount = commentHistory?.read else {return}
        
        let decrementPostRef = Database.database().reference().child("user-comment").child(uid)
        decrementPostRef.runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
            if var currentCountData = currentData.value as? [String: Any] {
                var countData = currentCountData["totalCount"] as! Int
                
                if countData == 0 {
                    return TransactionResult.abort()
                }
                
                print("this is countData", countData)
                
                countData -= readCount
                currentCountData["totalCount"] = countData
                currentData.value = currentCountData
                
                return TransactionResult.success(withValue: currentData)
            }
            return TransactionResult.success(withValue: currentData)
            
        }) { (err, commited, snapshot) in
            if let err = err {
                print("failed to decrement count", err)
            }
            
            self.decrementCommentCount()
            print("this is totalCount", snapshot?.value)
            
        }
    }
    
    func decrementCommentCount() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let toId = commentHistory?.toId else {return}
        guard let postUser = commentHistory?.postUser else {return}
        guard let postId = commentHistory?.id else {return}
        
        let decrementPostRef = Database.database().reference().child("user-comment").child(uid).child(toId).child(postUser).child(postId)
        decrementPostRef.runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
            if var currentCountData = currentData.value as? [String: Any] {
                var countData = currentCountData["read"] as? UInt
                
                if countData == 0 {
                    return TransactionResult.abort()
                }
                
                print("this is countData", countData)
                
                countData = 0
                currentCountData["read"] = countData
                currentData.value = currentCountData
                
                return TransactionResult.success(withValue: currentData)
            }
            return TransactionResult.success(withValue: currentData)
            
        }) { (err, commited, snapshot) in
            if let err = err {
                print("failed to decrement count", err)
            }
            
            print("this is decremented read count", snapshot?.value)
            
        }
    }
    
    func decrementHomeBadge(readCount: Int) {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        //guard let count = commentHistory?.read else {return}
        
        let decrementPostRef = Database.database().reference().child("count").child(uid)
        decrementPostRef.runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
            if var currentCountData = currentData.value as? [String: Any] {
                var countData = currentCountData["read"] as! Int
                
                if countData == 0 {
                    return TransactionResult.abort()
                }
                
                print("this is countData", countData)
                
                countData -= readCount
                currentCountData["read"] = countData
                currentData.value = currentCountData
                
                return TransactionResult.success(withValue: currentData)
            }
            return TransactionResult.success(withValue: currentData)
            
        }) { (err, commited, snapshot) in
            if let err = err {
                print("failed to decrement count", err)
            }
            
            print("this is totalCount", snapshot?.value)
            guard let dictionary = snapshot?.value as? [String: Any] else {return}
            guard let badgeCount = dictionary["read"] as? UInt else {return}
            
            print("badgeCount", badgeCount)
            
            UIApplication.shared.applicationIconBadgeNumber = Int(badgeCount)
            
        }
    }
    
    //    func decrementIfChange() {
    //
    ////        guard let uid = Auth.auth().currentUser?.uid else {return}
    ////        guard let toId = commentHistory?.toId else {return}
    ////        guard let postUser = commentHistory?.postUser else {return}
    ////        guard let postId = commentHistory?.id else {return}
    //
    //            handleDecrementRef = decrementRef.observe(.childChanged, with: { (snapshot) in
    //                print("child changed man!")
    //            guard let dictionary = snapshot.value as? [String: Any] else {return}
    //
    //                print("data after childadded", snapshot.value)
    //            if let read = dictionary["read"] as? Int {
    //                self.decrementTotalCount(readCount: read)
    //                self.decrementHomeBadge(readCount: read)
    //            }
    //        }) { (err) in
    //            print("failed to fetch read data", err.localizedDescription)
    //            return
    //        }
    //    }
    
    
    
    var comments = [Comment]()
    
    fileprivate func fetchComments() {
        
        guard let postId = self.post?.id ?? commentHistory?.id else {return}
        let ref = Database.database().reference().child("comments").child(postId)
        ref.observe(.childAdded, with: { (snapshot) in
            
              self.collectionView?.refreshControl?.endRefreshing()
            
            guard let dictionary = snapshot.value as? [String: Any] else {return}
            
            guard let uid = dictionary["uid"] as? String else {return}
            Database.fetchUserWithUID(uid: uid, completion: { (user) in
                
               // let comment = Comment(dictionary: dictionary)
                let comment = Comment(user: user, dictionary: dictionary)
                //comment.user = user
                self.comments.append(comment)
                self.comments.sort(by: { (c1, c2) -> Bool in
                    return c1.creationDate.compare(c2.creationDate) == .orderedAscending
                })
                self.collectionView.reloadData()
            })
        
            
        }) { (err) in
            print("failed to observe comments")
        }
    }
    
    fileprivate func fetchComment() {
        
         guard let postId = self.post?.id ?? commentHistory?.id else {return}
        let ref = Database.database().reference().child("comments").child(postId)
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
            self.collectionView?.refreshControl?.endRefreshing()
            
            guard let dictionary = snapshot.value as? [String: Any] else {return}
            dictionary.forEach({ (key, value) in
            
                guard let commentDictionary = value as? [String: Any] else {return}
                guard let uid = commentDictionary["uid"] as? String else {return}
                
                Database.fetchUserWithUID(uid: uid, completion: { (user) in
                    let comment = Comment(user: user, dictionary: commentDictionary)
                    self.comments.append(comment)
                    self.comments.sort(by: { (c1, c2) -> Bool in
                        return c1.creationDate.compare(c2.creationDate) == .orderedAscending
                    })
                    
                    self.collectionView.reloadData()
                })
                
            })
        }) { (err) in
            print("failed to fetch post comment", err.localizedDescription)
            return
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return comments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 50)
        let dummyCell = CommentCell(frame: frame)
        dummyCell.comment = comments[indexPath.item]
        dummyCell.layoutIfNeeded() //need for adjusting size
        
        let targetSize = CGSize(width: view.frame.width, height: 1000)
        let estimatedSize = dummyCell.systemLayoutSizeFitting(targetSize)
        
        let height = max(40 + 8 + 8, estimatedSize.height)
        return CGSize(width: view.frame.width, height: height) 
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CommentCell
        cell.comment = self.comments[indexPath.item]
        return cell
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
        //decrementCommentCount()
        //decrementTotalCount()
        //decrementHomeBadge()
        print("view appreared here")
        decrementAfterRefreshing()
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    
    lazy var containerView: CommentInputAccessoryView = {
        let frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 50)
        let commentInputAccessoryView = CommentInputAccessoryView(frame: frame)
        //brige to commentInputAcceeory view delegate
        commentInputAccessoryView.delegate = self
        return commentInputAccessoryView
    
    }()
    
    func didSubmit(for comment: String) {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let postId = self.post?.id ?? commentHistory?.id else {return}
        guard let toId = post?.user.uid ?? commentHistory?.toId else {return}
        guard let postUser = post?.user.uid ?? commentHistory?.postUser else {return}
        
        print("uid", uid, "postId", postId, "toId", toId)
        
        let value = ["text": comment, "creationDate": Date().timeIntervalSince1970, "uid": uid, "toId": toId, "postUser": postUser] as [String : Any]
        
        let ref = Database.database().reference().child("comments").child(postId)
        ref.childByAutoId().updateChildValues(value) { (err, ref) in
            if let err = err {
                print("failed to insert comments:", err.localizedDescription)
                return
            }
            
            print("Successfully inserted comments")
            self.containerView.commentClearTextField()
            self.comments.removeAll()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                 self.fetchComment()
            })
           
        }
        
    }
    
    override var inputAccessoryView: UIView? {
        get {
           return containerView
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
}
