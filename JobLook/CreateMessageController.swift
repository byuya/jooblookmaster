//
//  CreateMessageController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/09.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class CreateMessageController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var message: Message?
    let cellId = "cellId"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    collectionView.backgroundColor = .white
    collectionView.alwaysBounceVertical = true
     navigationItem.title = "新規メッセージ"
        
    collectionView.register(CreateMessageCell.self, forCellWithReuseIdentifier: cellId)
        
         fetchUsersForNewMessages()
        
        fetchNewMessageUsers()
       
    }
    var users = [User]()
    var messages = [Message]()
    
    fileprivate func fetchUsersForNewMessages() {
        print(123)
        //ここでMessengeControllerのメソッドを使いじじ受信トレイ
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        let ref = Database.database().reference().child("user-messages").child(uid)
        ref.observe(.childAdded, with: { (snapshot) in
            
            let userId = snapshot.key
            //print("show userId here:", userId)
            
            Database.database().reference().child("user-messages").child(uid).child(userId).observe(.childAdded, with: { (snapshot) in
                
                //print("get down to nodes:", snapshot)
                
                let messageId = snapshot.key
                
                let messagesReference = Database.database().reference().child("messages").child(messageId)
                messagesReference.observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    //print("show ids:", snapshot.value)
                    
                    if let dictionary = snapshot.value as? [String: Any] {
                        Database.fetchUserWithUID(uid: userId, completion: { (user) in
                            var message = Message(user: user, dictionary: dictionary)
                            
                           // if let chatPartnerId = message.chatPartnerId() {
                                message.user = user
                                //self.messagesDictionary[chatPartnerId] = message
                                //self.messages = Array(self.messagesDictionary.values)
                                self.messages.append(message)
                                
                            //}
                            
                            self.collectionView.reloadData()
                            
                        })
                    }
                    
                    
                    //                    guard let dictionary = snapshot.value as? [String: Any] else {return}
                    //
                    //                    //print(snapshot.value)
                    //
                    //                    guard let toMessageId = dictionary["toId"] as? String else {return}
                    //                    guard let fromMessageId = dictionary["fromId"] as? String else {return}
                    //                    //guard let messageText = dictionary["messageText"] as? String else {return}
                    //
                    //                    Database.fetchUserWithUID(uid: fromMessageId, completion: { (user) in
                    //                        if toMessageId != uid {
                    //                            print("omit myself from list")
                    //                            return
                    //                        }
                    //                        var message = Message(dictionary: dictionary)
                    //                       if let toId = message.toId {
                    //                            //message.user = user
                    //                            self.messagesDictionary[toId] = message
                    //                            self.messages = Array(self.messagesDictionary.values)
                    //                            //self.messages.append(message)
                    //                       }
                    //
                    //                        self.timer?.invalidate()
                    //                        print("we just canceled out timer")
                    //                        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.handleReloadTable), userInfo: nil, repeats: false)
                    //                        print("Schedule a table reload on 0.1 sec")
                    //
                    //                    })
                    
                }, withCancel: { (err) in
                    print("err", err)
                })
                
            }, withCancel: { (err) in
                print("failed to get userId info")
            })
            
        }) { (err) in
            print("failed to get user-messages", err)
        }
    }
    
    fileprivate func fetchNewMessageUsers() {
        print("getting users for creating new messages")

        let ref = Database.database().reference().child("users")
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            //print(snapshot.value)

            guard let dictionaries = snapshot.value as? [String: Any] else {return}
            dictionaries.forEach({ (key, value) in
                //print(key, value)

                if key == Auth.auth().currentUser?.uid {
                    print("You cannot send yo yourself uh???")
                    return
                }

                guard let userDictionary = value as? [String: Any] else {return}

                let user = User(uid: key, dictionary: userDictionary)
                self.users.append(user)
                self.collectionView.reloadData()
                //print(user.uid, user.username)

             })
        }) { (err) in
            print("failed to get users")
    
        }
    }
        
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CreateMessageCell
        cell.message = messages[indexPath.item]
        
        return cell
    }
   
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let message = messages[indexPath.item]

        let messageController = MessageController(collectionViewLayout: UICollectionViewFlowLayout())
        messageController.userId = message.chatPartnerId()
        messageController.message = message
        navigationController?.pushViewController(messageController, animated: true)
        
        
//        let message = messages[indexPath.item]
//
//        let messageController = MessageController(collectionViewLayout: UICollectionViewFlowLayout())
//        messageController.userId = message.chatPartnerId()
//        messageController.message = message
//        navigationController?.pushViewController(messageController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 60)
    }
    
}
