//
//  InboxUserCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/04.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

class InboxUserCell: UICollectionViewCell {
    
    var message: Message? {
        didSet {
            
            guard let message = message else {return}
            guard let profileImageUrl = message.user?.profileImageUrl else {return}
            
            
            messageLabel.text = message.messageText

            usernameLabel.text = message.user?.username
            
            profileViewImage.loadImage(urlString: profileImageUrl)
            
             let timeAgoDisplay = message.creationDate.timeAgoDisplay()
            dateLabel.text = timeAgoDisplay
            
//            guard let count = message.countNumber else {return}
//            badgeLabel.text = String(UInt(count))
            
            guard let count = message.read else {return}
            
            if count == 0 {
                badgeLabel.backgroundColor = .white
            } else {
                badgeLabel.backgroundColor = .green
                //print("counting in the cell", count)
                badgeLabel.text = String(UInt(count))
            }
        }
        
    }

    let profileViewImage: CustomImageView = {
        let iv = CustomImageView()
        iv.backgroundColor = .red
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    let usernameLabel: UILabel = {
        let label = UILabel()
        label.text = "Username"
        label.font = UIFont.boldSystemFont(ofSize: 16)
        return label
    }()
    
    let messageLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "Hi, this is a message"
        return label
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 12)
        label.text = "DD:MM:SS"
        return label
    }()
    
    let badgeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 0, alpha: 0.5)
        label.backgroundColor = .red
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = ""
        label.textAlignment = .center
        label.textColor = .white
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(profileViewImage)
        profileViewImage.anchor(top: nil, left: leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, height: 50, width: 50)
        profileViewImage.layer.cornerRadius = 50 / 2
        profileViewImage.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        addSubview(usernameLabel)
        usernameLabel.anchor(top: topAnchor, left: profileViewImage.rightAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 8, paddingBottom: 18, paddingRight: 0, height: 0, width: 0)
        
        addSubview(messageLabel)
        messageLabel.anchor(top: usernameLabel.bottomAnchor, left: profileViewImage.rightAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: -16, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(dateLabel)
        dateLabel.anchor(top: topAnchor, left: nil, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 18, paddingRight: 8, height: 0, width: 0)
        
        addSubview(badgeLabel)
        badgeLabel.anchor(top: dateLabel.bottomAnchor, left: nil, bottom: nil, right: rightAnchor, paddingTop: -10, paddingLeft: 0, paddingBottom: 0, paddingRight: 8, height: 25, width: 25)
        badgeLabel.layer.cornerRadius = 25 / 2
        badgeLabel.layer.masksToBounds = true
        
        let separatotView = UIView()
        separatotView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        addSubview(separatotView)
        separatotView.anchor(top: nil, left: usernameLabel.leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0.5, width: 0)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
