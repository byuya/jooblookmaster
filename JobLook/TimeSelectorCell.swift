//
//  TimeSelectorCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/17.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit


class TimeSelectorCell: UICollectionViewCell {
        
    var time: Time? {
        didSet {
            
           //textView.text = "this is will be time"
            //textView.text = handleSelectedTime()
            //textView.text = "Time Goes Here"
        }
    }
    let fromTextView: UITextView = {
        let tv = UITextView()
        tv.text = "fromTextView"
        tv.backgroundColor = UIColor.magenta
        tv.isEditable = false
        tv.inputView?.tag = 0
        tv.tag = 0
        return tv
    }()
    
    let toTextView: UITextView = {
        let tv = UITextView()
        tv.text = "toTextView"
        tv.backgroundColor = .blue
        tv.isEditable = false
        tv.inputView?.tag = 1
        tv.tag = 1
        return tv
    }()
    
    let label: UILabel = {
        let label = UILabel()
        label.text = "label"
        label.font = UIFont.systemFont(ofSize: 20)
        label.textColor = UIColor.black
        return label
        
    }()
    
    lazy var fromTimePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .time
        //picker.minuteInterval = 10
        picker.minuteInterval = 30
        picker.addTarget(self, action: #selector(handleFromSelectedTime), for: .valueChanged)
        return picker
    }()
    
    lazy var toTimePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .time
        //picker.minuteInterval = 10
        picker.minuteInterval = 30
        picker.addTarget(self, action: #selector(handleToSelectedTime), for: .valueChanged)
        return picker
    }()
    
    @objc func handleFromSelectedTime() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
       let selectTime = dateFormatter.string(from: fromTimePicker.date)
        fromTextView.tag = 0
//        fromTextView.inputView?.tag = 0
//        toTextView.inputView?.tag = 1
        return selectTime
    }
    
    @objc func handleToSelectedTime() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        let selectTime = dateFormatter.string(from: toTimePicker.date)
        toTextView.tag = 0
        //        fromTextView.inputView?.tag = 0
        //        toTextView.inputView?.tag = 1
        return selectTime
    }
    
    
//    @objc func handleSelectedTime() {
//        let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "h:mm a"
//        let selectTime = dateFormatter.string(from: timePicker.date)
//        textView.inputView = timePicker
//        textView.text = selectTime
//        print("handleSelectedTime is working here")
//        delegate?.updateTime(for: self)
//    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
       // print(TimeSetting.from.timeTextFunction())
        
//        addSubview(fromTextView)
//        fromTextView.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
//
//        addSubview(toTextView)
//        toTextView.anchor(top: fromTextView.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
//
        addSubview(fromTextView)
        fromTextView.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
        
        addSubview(toTextView)
        toTextView.anchor(top: fromTextView.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
        
//        addSubview(label)
//        label.anchor(top: topAnchor, left: fromTextView.rightAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
