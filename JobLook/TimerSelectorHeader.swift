//
//  TimerSelectorHeader.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/09/05.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

class TimeSelectorHeader: UICollectionViewCell {
    
    
    let headerLabel: UILabel = {
        let label = UILabel()
        label.text = "headerlabel"
        label.font = UIFont.systemFont(ofSize: 20)
        label.textColor = UIColor.black
        //label.textAlignment = .left
        return label
        
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.lightGray
        
        addSubview(headerLabel)
        headerLabel.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
