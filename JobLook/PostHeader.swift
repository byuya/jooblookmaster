//
//  PostHeader.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/30.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

protocol PostHeaderDelegate {
    func didTapBack()
}
class PostHeader: UICollectionViewCell {
    
    var delegate: PostHeaderDelegate?
    
    var post: Post?
    
    lazy var dismissButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "right_arrow_shadow").withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(handleDismissPost), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handleDismissPost() {
        delegate?.didTapBack()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .yellow
        addSubview(dismissButton)
        dismissButton.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
