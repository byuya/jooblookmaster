//
//  ChooseDateCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/10/14.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import FSCalendar
import CalculateCalendarLogic

protocol ChooseDateCellDelegate {
    func didTapChoose(for: ChooseDateCell)
}
class ChooseDateCell: UICollectionViewCell,  FSCalendarDelegate, FSCalendarDataSource {
    
    var schedule: Schedule? {
        didSet {
            guard let schedule = schedule else {return}
            titleLabel.text = schedule.time
        }
    }
    
    let chooseDateController = ChooseDateController()
    
    var delegate: ChooseDateCellDelegate?
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "11:00-13:00"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.backgroundColor = UIColor.gray
        return label
    }()
    
    lazy var decideButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Choose", for: .normal)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 50 / 3
        //button.titleLabel?.textAlignment = .right
        button.addTarget(self, action: #selector(handleDecide), for: .touchUpInside)
        button.backgroundColor = .white
        return button
        
    }()
    
    @objc fileprivate func handleDecide() {
        print("This will take you to terms page")
        
        delegate?.didTapChoose(for: self)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.red
        
        setupView()
    
    }
    
    func setupView() {
        
        addSubview(titleLabel)
        titleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 80, width: 0)
        
        addSubview(decideButton)
        decideButton.anchor(top: titleLabel.topAnchor, left: nil, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 20, height: 50, width: 50)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
