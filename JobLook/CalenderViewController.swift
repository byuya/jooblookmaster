//
//  CalenderViewControler.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/15.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

//import UIKit
//
//class CalenderViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, CalenderViewHeaderDelegate {
//
//    let cellId = "cellId"
//    let headerId = "headerId"
//
//    let dateManager = DateManager()
//    let calenderViewHeader = CalenderViewHeader()
//
//    var selectedDate = Date()
//
//    let cellMargin: CGFloat = 2.0
//    let daysPerWeek: Int = 7
//    var today: NSDate!
//
//    let weekArray: Array = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        collectionView.backgroundColor = .green
//
//        collectionView.delegate = self
//        collectionView.dataSource = self
//
//        collectionView.register(CalenderViewCell.self, forCellWithReuseIdentifier: cellId)
//
//        collectionView.register(CalenderViewHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerId)
//    }
//
//    func changeHeaderTitle() -> String {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "M/yyyy"
//        let selectMonth = formatter.string(from: selectedDate)
//        return selectMonth
//    }
//
//    func didTapPrevMonthButton() {
//
//        print("prevmonthbutton")
//
//        selectedDate = dateManager.prevMonth(date: selectedDate)
//        self.collectionView.reloadData()
//        calenderViewHeader.headerTitleMonthLabel.text = changeHeaderTitle()
//        //print(calenderViewHeader.headerTitleMonthLabel.text)
//
//    }
//
//    func didTapNextMonthButton() {
//        print("nextmonthButton")
//
//        selectedDate = dateManager.nextMonth(date: selectedDate)
//        self.collectionView.reloadData()
//        calenderViewHeader.headerTitleMonthLabel.text = changeHeaderTitle()
//    }
//
//    //var header: CalenderViewHeader?
//
//    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//
//        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as! CalenderViewHeader
//
//        header.headerTitleMonthLabel.text = changeHeaderTitle()
//
//        if indexPath.section == 1 {
//            header.isHidden = true
//
//        } else {
//
//           header.isHidden = false
//            header.delegate = self
//
//        }
//        return header
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        if section == 1 {
//            return CGSize(width: view.frame.width, height: 1.5)
//        }
//        return CGSize(width: view.frame.width, height: 50)
//    }
//
//    //var header: CalenderViewHeader?
//
//    override func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 2
//    }
//
//    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//
//        if section == 0 {
//
//            return 7
//        } else {
//            return dateManager.daysAcquisition()
//        }
//
//    }
//
//     //var header: CalenderViewHeader?
//    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CalenderViewCell
//
//        if (indexPath.row % 7 == 0) {
//            cell.textLabel.textColor = UIColor.red
//        } else if (indexPath.row % 7 == 6){
//            cell.textLabel.textColor = UIColor.blue
//        } else {
//            cell.textLabel.textColor = UIColor.lightGray
//        }
//
//        if indexPath.section == 0 {
//
//            cell.textLabel.text = weekArray[indexPath.row]
//
//        } else {
//            cell.textLabel.text = dateManager.conversionDateFormat(indexPath: indexPath)
//        }
//
//        return cell
//    }
//
//
//    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        print(selectedDate, dateManager.conversionDateFormat(indexPath: indexPath))
//        if indexPath.section == 0 {
//            return
//
//        }
//        //let timeSelectorController = TimeController(collectionViewLayout: UICollectionViewFlowLayout())
//        let timeSelectorController = TimeSelectorController(collectionViewLayout: UICollectionViewFlowLayout())
//        let navController = UINavigationController(rootViewController: timeSelectorController)
//        present(navController, animated: true, completion: nil)
//
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return cellMargin
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return cellMargin
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let numberMargin: CGFloat = 8.0
//        let width: CGFloat = (collectionView.frame.size.width - cellMargin * numberMargin) / CGFloat(daysPerWeek)
//        let height: CGFloat = width * 1.0
//        return CGSize(width: width, height: height)
//    }
//
//
////    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
////        return CGSize(width: view.frame.width, height: 50)
////    }
//
//
//}
