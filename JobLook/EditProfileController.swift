//
//  EditProfileController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/12.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import Stripe

class EditProfileController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let cellId = "cellId"
   
    var total: Int?
    var fundId: String?
    var fundIdArray = [String?]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .orange
        collectionView.alwaysBounceVertical = true
        
        collectionView.register(EditProfileCell.self, forCellWithReuseIdentifier: cellId)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "History", style: .plain, target: self, action: #selector(handleDeposit))
        
        fetchSales()
    }
    
    var fundHistory = [Fund]()
    
    @objc func handleDeposit() {

        let depositHistoryController = DepositHistoryController(collectionViewLayout: UICollectionViewFlowLayout())
        //depositController.total = total
        depositHistoryController.fundHistory = fundHistory
        navigationController?.pushViewController(depositHistoryController, animated: true)
    }
    
    var funds = [Fund]()
    var totalArray = [Int]()

    func fetchSales() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        let salesRef = Database.database().reference().child("users").child(uid).child("funds")
        salesRef.observe(.childAdded, with: { (snapshot) in
            
            //self.fundId = snapshot.key
            print("let me see this key", snapshot.key)
            self.fundIdArray.append(snapshot.key)
            guard let dictionary = snapshot.value as? [String: Any] else {return}
            Database.fetchUserWithUID(uid: uid, completion: { (user) in
                
                var fund = Fund(user: user, dictionary: dictionary)
                print("fund data", fund)
            
                fund.id = snapshot.key
                
                if fund.payoutProcess == 1 {
                    self.fundHistory.append(fund)
                    return
                }
                
                self.funds.append(fund)
                self.totalArray.append(fund.amount)
                
                let sum = self.totalArray.reduce(0, {sum, number in sum + number})
                print("this is total sum", sum)
                self.total = sum
                //fund.totalofAmount = sum
                self.collectionView.reloadData()
                self.setupInputView()
            })
            
        }) { (err) in
            print("failed to fetch data", err)
        }
    }
    
    @objc func handleToNextPage() {
        
        print("check check", total ?? 0)
        
        let depositController = DepositController(collectionViewLayout: UICollectionViewFlowLayout())
        depositController.total = total
        depositController.fundArraIds = fundIdArray
        navigationController?.pushViewController(depositController, animated: true)
        
    }
    
    func setupInputView() {
        
        let containerView: UIView = {
            let containerView = UIView()
            containerView.backgroundColor = UIColor.mainBlue()
            containerView.frame = CGRect(x: 0, y: 0, width: 100, height: 50)
            
            
            let totalLabel = UILabel()
            totalLabel.text = "Total:"
            totalLabel.font = UIFont.systemFont(ofSize: 20)
            totalLabel.textColor = .white
            totalLabel.textAlignment = .left
            containerView.addSubview(totalLabel)
            totalLabel.anchor(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
            
            if let priceText = total {
                let priceInt = String(priceText)
                
                let priceLabel = UILabel()
                priceLabel.text = "¥\(priceInt)"
                priceLabel.font = UIFont.systemFont(ofSize: 20)
                priceLabel.textColor = .white
                priceLabel.textAlignment = .left
                containerView.addSubview(priceLabel)
                priceLabel.anchor(top: containerView.topAnchor, left: totalLabel.rightAnchor, bottom: containerView.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
            }
            
            
            
            let contactButton = UIButton(type: .system)
            contactButton.setTitle("Request Deposit", for: .normal)
            //contactButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            //contactButton.titleLabel?.backgroundColor = .white
            contactButton.backgroundColor = .white
            contactButton.tintColor = UIColor(white: 0, alpha: 0.2)
            contactButton.layer.masksToBounds = true
            contactButton.layer.cornerRadius = 5
            contactButton.addTarget(self, action: #selector(handleToNextPage), for: .touchUpInside)
            containerView.addSubview(contactButton)
            contactButton.anchor(top: containerView.topAnchor, left: nil, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, paddingTop: 5, paddingLeft: 0, paddingBottom: 5, paddingRight: 10, height: 30, width: 150)
            
            return containerView
        }()
        
        view.addSubview(containerView)
        containerView.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: view.frame.width)
        
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! EditProfileCell
        cell.fund = funds[indexPath.item]
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return funds.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 120)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //navigationController?.navigationBar.isHidden = true
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
        
    }
    

}
