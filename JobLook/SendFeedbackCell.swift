//
//  SendFeedbackCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on 10/27/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit

protocol SendFeedbackCellDelegate {
    func didTapFeedback()
    func didTapDismiss()
    func didTapBackToRoot()
}
class SendFeedbackCell: UICollectionViewCell {
    
    var delegate: SendFeedbackCellDelegate?
    
    var experiment: Experiment? {
        didSet {
            titleLabel.text = experiment?.title
        }
    }
    
    lazy var dismissButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "right_arrow_shadow").withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(handleDismissPost), for: .touchUpInside)
        return button
    }()
    
    @objc func handleDismissPost() {
        print("handling dismiss")
        delegate?.didTapDismiss()
        
    }
    
    let feedbackLabel: UILabel = {
        let label = UILabel()
        label.text = "How was the experiment?"
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.backgroundColor = UIColor.gray
        return label
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Tour Title"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.backgroundColor = UIColor.gray
        return label
    }()
    
    
    lazy var giveFeedBackButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Submit Feedback", for: .normal)
        button.backgroundColor = UIColor.magenta
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handleGiveFeedBack), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handleGiveFeedBack() {
        delegate?.didTapFeedback()
        
    }
    
    lazy var backToRootButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Back to Root", for: .normal)
        button.backgroundColor = UIColor.cyan
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handleBackToRoot), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handleBackToRoot() {
        delegate?.didTapBackToRoot()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .yellow
        
        setupView()
    }
    
    func setupView() {
        
        addSubview(dismissButton)
        dismissButton.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(feedbackLabel)
        feedbackLabel.anchor(top: dismissButton.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 40, width: 0)
        
        addSubview(titleLabel)
        titleLabel.anchor(top: feedbackLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(giveFeedBackButton)
        giveFeedBackButton.anchor(top: titleLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 50, width: 0)
        
        addSubview(backToRootButton)
        backToRootButton.anchor(top: giveFeedBackButton.bottomAnchor, left: giveFeedBackButton.leftAnchor, bottom: nil, right: giveFeedBackButton.rightAnchor, paddingTop: 20, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
