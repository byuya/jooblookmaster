//
//  AddCardHeader.swift
//  JobLook
//
//  Created by Akiya Ozawa on 10/29/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit
import UserNotifications

protocol AddCardHeaderDelegate {
    func didTapHeaderDismiss()
    func didTapHeaderHandleAddCard(for cardNumber: String, for expYearNumber: String, for expMonthNumber: String, for cvc: String)
    func didTapCheck(for: AddCardCell)
}
class AddCardHeader: UICollectionViewCell {
    
    let addCardCell = AddCardCell()
    
    var delegate: AddCardHeaderDelegate?
    
    lazy var dismissButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("X", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.font = UIFont.systemFont(ofSize: 30)
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handleDismiss() {
        print("is dismiss working?")
        delegate?.didTapHeaderDismiss()
    }
    
    lazy var numberTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Card Number"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        tf.keyboardType = UIKeyboardType.numberPad
        return tf
    }()
    
    lazy var ccvTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "CCV"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        tf.keyboardType = UIKeyboardType.numberPad
        return tf
    }()
    
    lazy var expMonthTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Exp(month)"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        tf.keyboardType = UIKeyboardType.numberPad
        return tf
    }()
    
    lazy var expYearTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Exp(year)"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        tf.keyboardType = UIKeyboardType.numberPad
        return tf
    }()
    
    @objc fileprivate func handleTextInputChange() {
        
        let formIsValid = numberTextField.text?.count ?? 0 > 0 && expYearTextField.text?.count ?? 0 > 0 && expMonthTextField.text?.count ?? 0 > 0 && ccvTextField.text?.count ?? 0 > 0
        
        if formIsValid {
            addButton.isEnabled = true
            addButton.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
            
        } else {
            addButton.isEnabled = false
            addButton.backgroundColor = UIColor.lightGray
        }
    }
    
    lazy var addButton: UIButton = {
        let button = UIButton()
        button.setTitle("Add Card", for: .normal)
        button.backgroundColor = UIColor.lightGray
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.white, for: .normal)
        button.isEnabled = false
        button.addTarget(self, action: #selector(handleHeaderAddCreditCard), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handleHeaderAddCreditCard() {
        
        guard let cardNumberText = numberTextField.text else {return}
        guard let expYearText = expYearTextField.text else {return}
        guard let expMonthText = expMonthTextField.text else {return}
        guard let cvcText = ccvTextField.text else {return}
    
        delegate?.didTapHeaderHandleAddCard(for: cardNumberText, for: expYearText, for: expMonthText, for: cvcText)
       
    }
    
    lazy var checkButton: UIButton = {
        let button = UIButton()
        button.setTitle("Check fetch", for: .normal)
        button.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(handleCheck), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handleCheck() {
        delegate?.didTapCheck(for: addCardCell)
    }
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        setupView()
    }
    
    func setupView() {
        
        addSubview(dismissButton)
        dismissButton.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(numberTextField)
        numberTextField.anchor(top: dismissButton.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 30, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 0, width: 0)
        
        addSubview(expYearTextField)
        expYearTextField.anchor(top: numberTextField.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 30, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(expMonthTextField)
        expMonthTextField.anchor(top: numberTextField.bottomAnchor, left: expYearTextField.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 30, height: 0, width: 0)
        
        addSubview(ccvTextField)
        ccvTextField.anchor(top: expMonthTextField.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 0, width: 0)
        
        addSubview(addButton)
        addButton.anchor(top: ccvTextField.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 50, paddingBottom: 0, paddingRight: 50, height: 0, width: 0)
        
        addSubview(checkButton)
        checkButton.anchor(top: addButton.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 50, paddingBottom: 0, paddingRight: 50, height: 0, width: 0)
        
//        addSubview(addedCardLabel)
//        addedCardLabel.anchor(top: addButton.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
