//
//  CheckCommentController.swift
//  JobLook
//
//  Created by Akiya Ozawa on 1/28/31 H.
//  Copyright © 31 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth

class CheckCommentController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var ref: DatabaseReference!
    var handleRef: DatabaseHandle!
    
    var post: Post?
   
    let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Comment History"
        
        collectionView.backgroundColor = .orange
        
        collectionView.register(CheckCommentCell.self, forCellWithReuseIdentifier: cellId)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
        
        //fetchCommentCountDataAndItsPost()
        //fetchTest()
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        ref = Database.database().reference().child("user-comment").child(uid)
       
    }
    
    @objc func handleRefresh() {
        self.commentHistories.removeAll()
        
        fetchCommentTest()
        
    }
    
    var commentHistories = [CommentHistory]()
    
    func fetchCommentTest() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        //handleRef = ref.observe(.value, with: { (snapshot) in
            
        let reference = Database.database().reference().child("user-comment").child(uid)
        reference.observeSingleEvent(of: .value, with: { (snapshot) in
            
            self.commentHistories.removeAll()
            
            //print("snapshot", snapshot.value)
            self.collectionView?.refreshControl?.endRefreshing()
            
            guard let userCommentDict = snapshot.value as? [String: Any] else {return}
            userCommentDict.forEach({ (key, value) in
                //print("this is postId", value)
              
                guard let postIdDict = value as? [String: Any] else {return}
                postIdDict.forEach({ (postUser, postValue) in
                    print("shoud be postUser", postUser)
                    //print("comVal", postValue)
                    
                    guard let lastDictionary = postValue as? [String: Any] else {return}
                    
                    lastDictionary.forEach({ (postId, postVal) in
                        //self.childChangedAndDecrement(toId: key, postUser: postUser, postId: postId)
                        guard let dict = postVal as? [String: Any] else {return}
                        
                        Database.fetchUserWithUID(uid: key, completion: { (user) in
                            Database.fetchPostWithId(postUser: postUser, postId: postId, completion: { (testPost) in
                                
                                var commentHisotry = CommentHistory(user: user, testPost: testPost, dictionary: dict)
                                commentHisotry.toId = key
                                commentHisotry.id = postId
                                commentHisotry.postUser = postUser
                                
                                print("make if this is toid guest name", commentHisotry.user.username)
                                self.commentHistories.append(commentHisotry)
                            
                                //self.countArray.append(commentHisotry.read)
                                //
                                //print("numberArray", self.countArray)
                                //var sum = 0
                                //let lastIndex = self.countArray.count - 1
                                //for i in 0...lastIndex {
                                //let currentElement = self.countArray[i]
                                //sum += currentElement
                                //  }
                                //
                                //print("this is sum of numbers", sum)
                                
                                //let total = self.countArray.reduce(0, {total, number in total + number})
                                //print("this is count", total)
                                
                                DispatchQueue.main.async(execute: {
                                     self.collectionView.reloadData()
                                })
                        
                                //})
                                
                            })
                        })
                    
                    })
                    
                })
            })
            
        }) { (err) in
            print("failed to fetch commentData", err.localizedDescription)
            return
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CheckCommentCell
        cell.commentHistory = commentHistories[indexPath.item]
        return cell
      
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return commentHistories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //let commentHistory = commentHistories[indexPath.item]
        
        let commentsController = CommentsController(collectionViewLayout: UICollectionViewFlowLayout())
        commentsController.commentHistory = commentHistories[indexPath.item]
        navigationController?.pushViewController(commentsController, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         fetchCommentTest()
        tabBarController?.tabBar.isHidden = true
        
        //removeArray()
//         NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateCommentArray), name: UserProfileController.updateCommentHistoryArray, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeArray()
        //ref.removeObserver(withHandle: handleRef)
    }
    
    func removeArray() {
        self.commentHistories.removeAll()
        self.collectionView.reloadData()
        
    }
    
}

//    func  childChangedAndDecrement(toId: String, postUser: String, postId: String) {
//
//        guard let uid = Auth.auth().currentUser?.uid else {return}
//
//        let changeRef = Database.database().reference().child("user-comment").child(uid).child(toId).child(postUser)
//        changeRef.observe(.childChanged, with: { (snapshot) in
//             self.commentHistories.removeAll()
//            let key = snapshot.key
//
//            guard let dictionary = snapshot.value as? [String: Any] else {return}
//
//            Database.fetchUserWithUID(uid: uid, completion: { (user) in
//                Database.fetchPostWithId(postUser: postUser, postId: key, completion: { (testPost) in
//
//                    var commentHisotry = CommentHistory(user: user, testPost: testPost, dictionary: dictionary)
//                    commentHisotry.toId = toId
//                    commentHisotry.id = key
//                    commentHisotry.postUser = postUser
//
//                    self.commentHistories.append(commentHisotry)
//
//                    DispatchQueue.main.async(execute: {
//                        self.collectionView.reloadData()
//                    })
//
//                })
//            })
//        }) { (err) in
//            print("failed to fetch data", err.localizedDescription)
//            return
//        }
//    }

//    @objc fileprivate func handleUpdateCommentArray(notification: Notification) {
//
////        if let userInfo = notification.userInfo {
////            var newCommentHistories = userInfo["commentHistories"]
////
////            print("update comment count", newCommentHistories)
////            newCommentHistories = commentHistories
//
//            print("getting here")
//
//            self.collectionView.reloadData()
//
//        //}
//
//    }

