//
//  EvenDetailCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on 11/8/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit

class EventDetailCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.magenta
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
