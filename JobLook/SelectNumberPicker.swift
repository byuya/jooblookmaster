//
//  SelectNumberPicker.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/14.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

class SelectNumberPicker: UIPickerView {
    
    let categoryPickerView: UIPickerView = {
        let cv = UIPickerView()
        return cv
    }()
    
    let placePickerView: UIPickerView = {
        let pv = UIPickerView()
        return pv
    }()
    
    let languagePickerView: UIPickerView = {
        let lv = UIPickerView()
        return lv
    }()
    
    let participanstPickerView: UIPickerView = {
        let partView = UIPickerView()
        return partView
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        categoryPickerView.tag = 1
        placePickerView.tag = 2
        languagePickerView.tag = 3
        participanstPickerView.tag = 4
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = .brown
        self.layer.masksToBounds = true
        
//        addSubview(categoryPickerView)
//        categoryPickerView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
